package com.emxcel.beep.portlet;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

public class Service implements Serializable{

	public static void main(String s[]) throws ParseException {
		
		List<String> dates = new ArrayList<>();
		dates.add("2020-03-20T15:45:50.782Z");
		dates.add("2020-03-06T15:45:50.782Z");
		dates.add("2020-06-25T15:45:50.782Z");
		
		String dateInStr = "2020-03-20T15:45:50.782Z";
		
		String pattern = "yyyy-MM-dd'T'HH:mm:ssZ";
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	Date d1 = dateFormat.parse(dateInStr);
	System.out.println(dates.toString());
	 dates.sort((o1,o2) -> {
		try {
			return dateFormat.parse(o2).compareTo(dateFormat.parse(o1));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	});
	 System.out.println(dates.toString());

		
	}
}
