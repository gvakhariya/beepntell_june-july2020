package com.emxcel.beep.portlet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.osgi.service.component.annotations.Component;

import com.emxcel.beep.constants.BeepNTellPortletKeys;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * @author LENOVO
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=BeepNTell",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/index.jsp",
		"javax.portlet.name=" + BeepNTellPortletKeys.BEEPNTELL, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user", "com.liferay.portlet.requires-namespaced-parameters=false",
		"com.liferay.portlet.use-default-template=false" }, service = Portlet.class)

public class BeepNTellPortlet extends MVCPortlet {

	private Log log = LogFactoryUtil.getLog(this.getClass().getName());

	static List<Map<String, Object>> SalonDetails = new ArrayList<>();
	static List<Map<String, Object>> HospitalDetails = new ArrayList<>();
	static List<Map<String, Object>> GroceryDetails = new ArrayList<>();
	static List<Map<String, Object>> AllServiceDetails = new ArrayList<>();
	static List<String> activitiesList = new ArrayList<>();
	static List<Map<String, Object>> AllServiceDetailsActivityWise = new ArrayList<>();
	static List<Map<String, Object>> FilteredServiceDetails = new ArrayList<>();

	@SuppressWarnings("unchecked")
	@Override
	public void render(RenderRequest request, RenderResponse response) throws IOException, PortletException {
		System.out.println("rendering");
		if (SalonDetails.isEmpty()) {
			loadDataOnStartup(request);
		}
		List<Map<String, Object>> topOffersServices = new ArrayList<>();
		List<Map<String, Object>> topRecentReviewsServices = new LinkedList<>();
		List<Map<String, Object>> topTrendingReviewsServices = new LinkedList<>();
		List<Map<String, Object>> topOffersServicesForAllActvities = new ArrayList<>();
		List<Map<String, Object>> topRecentReviewsServicesForAllActvities = new LinkedList<>();
		List<Map<String, Object>> topTrendingReviewsServicesForAllActvities = new LinkedList<>();
		List<Map<String, Object>> selectedActivityServicesDetails = new LinkedList<>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

		try {

			for (String activity : activitiesList) {

				if (activity.equals("Salon"))
					selectedActivityServicesDetails = SalonDetails;
				else if (activity.equals("Hospital"))
					selectedActivityServicesDetails = HospitalDetails;
				else if (activity.equals("Grocery"))
					selectedActivityServicesDetails = GroceryDetails;

				topOffersServices = selectedActivityServicesDetails.stream()
						.sorted((o1, o2) -> ((Long) o2.get("Offer")).compareTo((Long) o1.get("Offer"))).limit(3)
						.collect(Collectors.toList());

				topOffersServices = transformFilteredServiceDetails(topOffersServices, activity);

				List<Map<String, Object>> allReviews = (List<Map<String, Object>>) selectedActivityServicesDetails
						.stream()
						.filter(map -> map.containsKey("ReviewComments")
								&& !(((List<Map<String, Object>>) map.get("ReviewComments")).isEmpty()))
						.flatMap(map -> ((List<Map<String, Object>>) map.get("ReviewComments")).stream()
								.filter(item -> item.containsKey("Like")))
						.collect(Collectors.toList());

				List<Long> sortTrendingReviewServiceIds = (List<Long>) allReviews.stream()
						.sorted((o1, o2) -> ((Long) o2.get("Like")).compareTo((Long) o1.get("Like"))).limit(3)
						.map(map -> (Long) map.get("ServiceId")).collect(Collectors.toList());

				topTrendingReviewsServices = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
						.filter(map -> map.containsKey("Id")
								&& sortTrendingReviewServiceIds.contains((Long) map.get("Id")))
						.collect(Collectors.toList());

				topTrendingReviewsServices.forEach(topTrendingService -> {
					((List<Map<String, Object>>) topTrendingService.get("ReviewComments"))
							.sort((o1, o2) -> ((Long) o2.get("Like")).compareTo((Long) o1.get("Like")));

				});
				topTrendingReviewsServices = transformFilteredServiceDetails(topTrendingReviewsServices, activity);

				List<Long> sortRecentReviewServiceIds = (List<Long>) allReviews.stream().sorted((o1, o2) -> {
					try {
						return ((Date) dateFormat.parse((String) o2.get("ReviewDate")))
								.compareTo((Date) dateFormat.parse((String) o1.get("ReviewDate")));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return 0;
				}).limit(3).map(map -> (Long) map.get("ServiceId")).collect(Collectors.toList());

				topRecentReviewsServices = (List<Map<String, Object>>) selectedActivityServicesDetails.stream().filter(
						map -> map.containsKey("Id") && sortRecentReviewServiceIds.contains((Long) map.get("Id")))
						.collect(Collectors.toList());

				topRecentReviewsServices.forEach(topRecentReviewsService -> {
					((List<Map<String, Object>>) topRecentReviewsService.get("ReviewComments")).sort((o1, o2) -> {
						try {
							return ((Date) dateFormat.parse((String) o2.get("ReviewDate")))
									.compareTo((Date) dateFormat.parse((String) o1.get("ReviewDate")));
						} catch (ParseException e) {
							e.printStackTrace();
						}
						return 0;
					});

				});
				topRecentReviewsServices = transformFilteredServiceDetails(topRecentReviewsServices, activity);

				topOffersServicesForAllActvities.add(topOffersServices.get(0));
				topRecentReviewsServicesForAllActvities.add(topRecentReviewsServices.get(0));
				topTrendingReviewsServicesForAllActvities.add(topTrendingReviewsServices.get(0));

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		request.setAttribute("topOffersServices", topOffersServicesForAllActvities);
		request.setAttribute("topRecentReviewsServices", topRecentReviewsServicesForAllActvities);
		request.setAttribute("topTrendingReviewsServices", topTrendingReviewsServicesForAllActvities);
		super.render(request, response);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		// move these to the class level at least and then you can use them in your jsp
		// page.
		final String OPERATION_ONE = "fetchSearchSuggestions";
		final String OPERATION_TWO = "filterListedServices";
		final String OPERATION_THREE = "filterReviewsForSelectedService";
		final String OPERATION_FOUR = "getOffersTrendingRecentReviewsDataForActivity";

		String resourceId = resourceRequest.getResourceID();
		String result = null;

		if (Validator.isNotNull(resourceId)) {
			switch (resourceId) {
			case OPERATION_ONE:
				result = fetchSearchSuggestions(resourceRequest, resourceResponse);
				break;
			case OPERATION_TWO:
				result = filterListedServices(resourceRequest, resourceResponse);
				break;
			case OPERATION_THREE:
				result = filterReviewsForSelectedService(resourceRequest, resourceResponse);
				break;
			case OPERATION_FOUR:
				result = getOffersTrendingRecentReviewsDataForActivity(resourceRequest, resourceResponse);
				break;
			default:
				log.warn("Unexpected resource id found: " + resourceId);
			}
		} else {
			// report back error that resource id was missing
		}
		resourceResponse.getPortletOutputStream().write(result.getBytes());
	}

	private String getOffersTrendingRecentReviewsDataForActivity(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws JsonGenerationException, JsonMappingException, IOException {
		String activityType = ParamUtil.getString(resourceRequest, "activityType");
		List<Map<String, Object>> topOffersServices = new ArrayList<>();
		List<Map<String, Object>> topRecentReviewsServices = new LinkedList<>();
		List<Map<String, Object>> topTrendingReviewsServices = new LinkedList<>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

		if (activityType == null || activityType == "") {

		}

		else {
			List<Map<String, Object>> selectedActivityServicesDetails = null;

			if (activityType.equals("Salon"))
				selectedActivityServicesDetails = SalonDetails;
			else if (activityType.equals("Hospital"))
				selectedActivityServicesDetails = HospitalDetails;
			else if (activityType.equals("Grocery"))
				selectedActivityServicesDetails = GroceryDetails;

			topOffersServices = selectedActivityServicesDetails.stream()
					.sorted((o1, o2) -> ((Long) o2.get("Offer")).compareTo((Long) o1.get("Offer"))).limit(3)
					.collect(Collectors.toList());

			List<Long> sortTrendingReviewServiceIds;
			List<Long> sortRecentReviewServiceIds;

			List<Map<String, Object>> allReviews = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
					.filter(map -> map.containsKey("ReviewComments")
							&& !(((List<Map<String, Object>>) map.get("ReviewComments")).isEmpty()))
					.flatMap(map -> ((List<Map<String, Object>>) map.get("ReviewComments")).stream()
							.filter(item -> item.containsKey("Like")))
					.collect(Collectors.toList());

			sortTrendingReviewServiceIds = (List<Long>) allReviews.stream()
					.sorted((o1, o2) -> ((Long) o2.get("Like")).compareTo((Long) o1.get("Like"))).limit(3)
					.map(map -> (Long) map.get("ServiceId")).collect(Collectors.toList());

			topTrendingReviewsServices = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
					.filter(map -> map.containsKey("Id") && sortTrendingReviewServiceIds.contains((Long) map.get("Id")))
					.collect(Collectors.toList());

			topTrendingReviewsServices.forEach(topTrendingService -> {
				((List<Map<String, Object>>) topTrendingService.get("ReviewComments"))
						.sort((o1, o2) -> ((Long) o2.get("Like")).compareTo((Long) o1.get("Like")));

			});

			sortRecentReviewServiceIds = (List<Long>) allReviews.stream().sorted((o1, o2) -> {
				try {
					return ((Date) dateFormat.parse((String) o2.get("ReviewDate")))
							.compareTo((Date) dateFormat.parse((String) o1.get("ReviewDate")));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return 0;
			}).limit(3).map(map -> (Long) map.get("ServiceId")).collect(Collectors.toList());

			topRecentReviewsServices = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
					.filter(map -> map.containsKey("Id") && sortRecentReviewServiceIds.contains((Long) map.get("Id")))
					.collect(Collectors.toList());

			topRecentReviewsServices.forEach(topRecentReviewsService -> {
				((List<Map<String, Object>>) topRecentReviewsService.get("ReviewComments")).sort((o1, o2) -> {
					try {
						return ((Date) dateFormat.parse((String) o2.get("ReviewDate")))
								.compareTo((Date) dateFormat.parse((String) o1.get("ReviewDate")));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return 0;
				});

			});

		}

		Map<String, Object> offersRecentTrendingData = new HashMap<>();
		offersRecentTrendingData.put("topOffersServices", topOffersServices);
		offersRecentTrendingData.put("topTrendingReviewsServices", topTrendingReviewsServices);
		offersRecentTrendingData.put("topRecentReviewsServices", topRecentReviewsServices);

		ObjectMapper objectMapper = new ObjectMapper();

		String json = objectMapper.writeValueAsString(offersRecentTrendingData);

		System.out.println(json);

		return json;

	}

	private String filterReviewsForSelectedService(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws JsonGenerationException, JsonMappingException, IOException {
		String activityType = ParamUtil.getString(resourceRequest, "activityType");
		Long serviceId = ParamUtil.getLong(resourceRequest, "serviceId");
		Long filterNumber = ParamUtil.getLong(resourceRequest, "filterNumber");
		List<Map<String, Object>> selectedServiceReviewComments = new ArrayList<>();
		ObjectMapper objectMapper = new ObjectMapper();
		List<Map<String, Object>> selectedActivityServicesDetails = null;
		// return activityType;
		try {
			if (activityType.equals("Salon"))
				selectedActivityServicesDetails = SalonDetails;
			else if (activityType.equals("Hospital"))
				selectedActivityServicesDetails = HospitalDetails;
			else if (activityType.equals("Grocery"))
				selectedActivityServicesDetails = GroceryDetails;

			selectedServiceReviewComments = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
					.filter(map -> ((Long) map.get("Id") == serviceId) && (map.containsKey("ReviewComments"))
							&& !(((List<Map<String, Object>>) map.get("ReviewComments")).isEmpty()))
					.flatMap(map -> ((List<Map<String, Object>>) map.get("ReviewComments")).stream()
							.filter(item -> item.containsKey("Like")))
					.collect(Collectors.toList());

			if (filterNumber == 1) {
				Map<String, Object> MostHelpfulReview = selectedServiceReviewComments.stream()
						.sorted((o1, o2) -> ((Long) o2.get("Like")).compareTo((Long) o1.get("Like"))).findFirst().get();
				selectedServiceReviewComments = new ArrayList<>();
				selectedServiceReviewComments.add(MostHelpfulReview);
			}

			if (filterNumber == 2) {
				selectedServiceReviewComments = selectedServiceReviewComments.stream()
						.filter(map -> (new Double(map.get("ReviewRating").toString())) >= 4.0)
						.collect(Collectors.toList());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String json = objectMapper.writeValueAsString(selectedServiceReviewComments);

		System.out.println(json);

		return json;
	}

	private String filterListedServices(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws JsonGenerationException, JsonMappingException, IOException {

		String activityType = ParamUtil.getString(resourceRequest, "activityType");
		String searchInput = ParamUtil.getString(resourceRequest, "searchText");
		List<String> ratingInputList = Arrays.asList(ParamUtil.getStringValues(resourceRequest, "ratingInput[]"));
		Double ratingInput = ratingInputList.isEmpty() ? 0.0
				: Double.valueOf(ratingInputList.stream().min(Comparator.comparing(String::valueOf)).get());
		List<String> cityInputList = Arrays.asList(ParamUtil.getStringValues(resourceRequest, "cityInput[]"));

		Map<String, Object> listingPageData = new HashMap<>();

		List<Map<String, Object>> filteredServiceDetails = new ArrayList<>();
		Map<Object, Long> cityMap = new HashMap<>();

		try {
			// @TODO: need to show all json data for all activities
			filteredServiceDetails = getFilteredServicesData(activityType, searchInput, ratingInput, cityInputList,
					filteredServiceDetails, true);

			// if search is found either by name or address, then transforming it
			// by adding new field 'activityType' in each serviceDetail along with sorting
			// and cityMap creation
			if (!filteredServiceDetails.isEmpty()) {
				filteredServiceDetails.sort((o1, o2) -> ((Long) o2.get("Offer")).compareTo((Long) o1.get("Offer")));
				cityMap = filteredServiceDetails.stream()
						.flatMap(map -> map.entrySet().stream().filter(item -> item.getKey().equals("City")))
						.collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.counting()));
			}

			listingPageData.put("cityMap", cityMap);
			listingPageData.put("filteredSearchServices", filteredServiceDetails);

			System.out.println(listingPageData.toString());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		ObjectMapper objectMapper = new ObjectMapper();

		String json = objectMapper.writeValueAsString(listingPageData);

		return json;

	}

	private List<Map<String, Object>> getFilteredServicesData(String activityType, String searchInput,
			Double ratingInput, List<String> cityInputList, List<Map<String, Object>> filteredServiceDetails,
			Boolean isOptionalFilterNeeded) {

		Predicate<Map<String, Object>> filter;
		List<Map<String, Object>> activityBasedfilteredServiceDetails;

		// @TODO: need to show all json data for all activities
		if (activityType == null || activityType == "") {
			filter = map -> ((String) map.get("Name")).toLowerCase().contains(searchInput.toLowerCase());
			if (isOptionalFilterNeeded) {
				filter = addOptionalFilters(filter, searchInput, ratingInput, cityInputList);
			}
			activityBasedfilteredServiceDetails = (List<Map<String, Object>>) SalonDetails.stream().filter(filter)
					.collect(Collectors.toList());
			activityBasedfilteredServiceDetails = transformFilteredServiceDetails(activityBasedfilteredServiceDetails,
					"Salon");
			filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

			activityBasedfilteredServiceDetails = (List<Map<String, Object>>) HospitalDetails.stream().filter(filter)
					.collect(Collectors.toList());
			activityBasedfilteredServiceDetails = transformFilteredServiceDetails(activityBasedfilteredServiceDetails,
					"Hospital");
			filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

			activityBasedfilteredServiceDetails = (List<Map<String, Object>>) GroceryDetails.stream().filter(filter)
					.collect(Collectors.toList());
			activityBasedfilteredServiceDetails = transformFilteredServiceDetails(activityBasedfilteredServiceDetails,
					"Grocery");
			filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

			// if No services found by Name, then finding by Address.
			if (filteredServiceDetails == null || filteredServiceDetails.isEmpty()) {

				filter = map -> ((String) map.get("Address")).toLowerCase().contains(searchInput.toLowerCase());
				if (isOptionalFilterNeeded) {
					filter = addOptionalFilters(filter, searchInput, ratingInput, cityInputList);
				}

				activityBasedfilteredServiceDetails = (List<Map<String, Object>>) SalonDetails.stream().filter(filter)
						.collect(Collectors.toList());
				activityBasedfilteredServiceDetails = transformFilteredServiceDetails(
						activityBasedfilteredServiceDetails, "Salon");
				filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

				activityBasedfilteredServiceDetails = (List<Map<String, Object>>) HospitalDetails.stream()
						.filter(filter).collect(Collectors.toList());
				activityBasedfilteredServiceDetails = transformFilteredServiceDetails(
						activityBasedfilteredServiceDetails, "Hospital");
				filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

				activityBasedfilteredServiceDetails = (List<Map<String, Object>>) GroceryDetails.stream().filter(filter)
						.collect(Collectors.toList());
				activityBasedfilteredServiceDetails = transformFilteredServiceDetails(
						activityBasedfilteredServiceDetails, "Grocery");
				filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);
			}

		} else {

			// show all json data for selected activities
			List<Map<String, Object>> selectedActivityServicesDetails = null;
			if (activityType.equals("Salon"))
				selectedActivityServicesDetails = SalonDetails;
			else if (activityType.equals("Hospital"))
				selectedActivityServicesDetails = HospitalDetails;
			else if (activityType.equals("Grocery"))
				selectedActivityServicesDetails = GroceryDetails;

			filter = map -> ((String) map.get("Name")).toLowerCase().contains(searchInput.toLowerCase());
			if (isOptionalFilterNeeded) {
				filter = addOptionalFilters(filter, searchInput, ratingInput, cityInputList);
			}
			filteredServiceDetails = (List<Map<String, Object>>) selectedActivityServicesDetails.stream().filter(filter)
					.collect(Collectors.toList());

			// if no search found by Name, finding with Address.
			if (filteredServiceDetails.isEmpty()) {
				filter = map -> ((String) map.get("Address")).toLowerCase().contains(searchInput.toLowerCase());
				if (isOptionalFilterNeeded) {
					filter = addOptionalFilters(filter, searchInput, ratingInput, cityInputList);
				}
				filteredServiceDetails = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
						.filter(filter).collect(Collectors.toList());
			}
			if (!filteredServiceDetails.isEmpty())
				filteredServiceDetails = transformFilteredServiceDetails(filteredServiceDetails, activityType);

		}
		return filteredServiceDetails;
	}

	private Predicate<Map<String, Object>> addOptionalFilters(Predicate<Map<String, Object>> filter, String searchText,
			Double ratingInput, List<String> cityInputList) {

		if (ratingInput != null && ratingInput != 0.0) {
			filter = filter.and(map -> (new Double(map.get("Rating").toString())) >= ratingInput);
		}

		if (cityInputList != null && !cityInputList.isEmpty()) {
			Predicate<Map<String, Object>> cityFilter = null;
			int i = 0;
			for (String cityInput : cityInputList) {
				if (i == 0) {
					cityFilter = (map -> ((String) map.get("City")).toLowerCase().contains(cityInput.toLowerCase()));
				} else {
					cityFilter = cityFilter
							.or(map -> ((String) map.get("City")).toLowerCase().contains(cityInput.toLowerCase()));
				}
				i++;
			}
			filter = filter.and(cityFilter);
		}
		return filter;
	}

	private String fetchSearchSuggestions(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws JsonGenerationException, JsonMappingException, IOException {
		String activityType = ParamUtil.getString(resourceRequest, "activityType");
		String searchInput = ParamUtil.getString(resourceRequest, "searchText");
		System.out.println("Activity selected : " + activityType);
		List<String> searchSuggestions = null;
		try {
			// @TODO: need to show all json data for all activities
			if (activityType == null || activityType == "") {
				searchSuggestions = new ArrayList<>();
				searchSuggestions = (List<String>) AllServiceDetails.stream()
						.filter(map -> ((String) map.get("Name")).toLowerCase().contains(searchInput.toLowerCase()))
						.map(map -> (String) map.get("Name")).collect(Collectors.toList());

				// if no search found for serviceName
				if (searchSuggestions.isEmpty()) {
					searchSuggestions = (List<String>) AllServiceDetails.stream().filter(
							map -> ((String) map.get("Address")).toLowerCase().contains(searchInput.toLowerCase()))
							.map(map -> (String) map.get("Name")).collect(Collectors.toList());
				}
			} else {
				// show all json data for selected activities
				List<Map<String, Object>> selectedActivityServicesDetails = null;
				if (activityType.equals("Salon"))
					selectedActivityServicesDetails = SalonDetails;
				else if (activityType.equals("Hospital"))
					selectedActivityServicesDetails = HospitalDetails;
				else if (activityType.equals("Grocery"))
					selectedActivityServicesDetails = GroceryDetails;

				searchSuggestions = new ArrayList<>();
				searchSuggestions = (List<String>) selectedActivityServicesDetails.stream()
						.filter(map -> ((String) map.get("Name")).toLowerCase().contains(searchInput.toLowerCase()))
						.map(map -> (String) map.get("Name")).collect(Collectors.toList());

				// if no search found for serviceName
				if (searchSuggestions.isEmpty()) {
					searchSuggestions = (List<String>) selectedActivityServicesDetails.stream().filter(
							map -> ((String) map.get("Address")).toLowerCase().contains(searchInput.toLowerCase()))
							.map(map -> (String) map.get("Name")).collect(Collectors.toList());
				}
			}
			searchSuggestions.sort((o1, o2) -> o2.compareTo(o1));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(searchSuggestions);
		System.out.println(" searchSuggestions ---" + json);
		return json;
	}

	@ProcessAction(name = "listingServices")
	public void listingServices(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		String activityType = ParamUtil.getString(request, "activityType");
		String searchInput = ParamUtil.getString(request, "searchText");

		// @TODO : based on activitytype, fetch the data.

		List<Map<String, Object>> filteredServiceDetails = new ArrayList<>();
		List<Map<String, Object>> activityBasedfilteredServiceDetails = new ArrayList<>();
		Map<Object, Long> cityMap = new HashMap<>();
		Predicate<Map<String, Object>> filter = null;

		// @TODO: need to show all json data for all activities
		if (activityType == null || activityType == "") {
			filter = map -> ((String) map.get("Name")).toLowerCase().contains(searchInput.toLowerCase());

			activityBasedfilteredServiceDetails = (List<Map<String, Object>>) SalonDetails.stream().filter(filter)
					.collect(Collectors.toList());
			activityBasedfilteredServiceDetails = transformFilteredServiceDetails(activityBasedfilteredServiceDetails,
					"Salon");
			filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

			activityBasedfilteredServiceDetails = (List<Map<String, Object>>) HospitalDetails.stream().filter(filter)
					.collect(Collectors.toList());
			activityBasedfilteredServiceDetails = transformFilteredServiceDetails(activityBasedfilteredServiceDetails,
					"Hospital");
			filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

			activityBasedfilteredServiceDetails = (List<Map<String, Object>>) GroceryDetails.stream().filter(filter)
					.collect(Collectors.toList());
			activityBasedfilteredServiceDetails = transformFilteredServiceDetails(activityBasedfilteredServiceDetails,
					"Grocery");
			filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

			// if No services found by Name, then finding by Address.
			if (filteredServiceDetails == null || filteredServiceDetails.isEmpty()) {

				filter = map -> ((String) map.get("Address")).toLowerCase().contains(searchInput.toLowerCase());

				activityBasedfilteredServiceDetails = (List<Map<String, Object>>) SalonDetails.stream().filter(filter)
						.collect(Collectors.toList());
				activityBasedfilteredServiceDetails = transformFilteredServiceDetails(
						activityBasedfilteredServiceDetails, "Salon");
				filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

				activityBasedfilteredServiceDetails = (List<Map<String, Object>>) HospitalDetails.stream()
						.filter(filter).collect(Collectors.toList());
				activityBasedfilteredServiceDetails = transformFilteredServiceDetails(
						activityBasedfilteredServiceDetails, "Hospital");
				filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);

				activityBasedfilteredServiceDetails = (List<Map<String, Object>>) GroceryDetails.stream().filter(filter)
						.collect(Collectors.toList());
				activityBasedfilteredServiceDetails = transformFilteredServiceDetails(
						activityBasedfilteredServiceDetails, "Grocery");
				filteredServiceDetails.addAll(activityBasedfilteredServiceDetails);
			}
		}

		// show services data for selected activities
		else {
			List<Map<String, Object>> selectedActivityServicesDetails = null;
			if (activityType.equals("Salon"))
				selectedActivityServicesDetails = SalonDetails;
			else if (activityType.equals("Hospital"))
				selectedActivityServicesDetails = HospitalDetails;
			else if (activityType.equals("Grocery"))
				selectedActivityServicesDetails = GroceryDetails;

			filter = map -> ((String) map.get("Name")).toLowerCase().contains(searchInput.toLowerCase());
			filteredServiceDetails = (List<Map<String, Object>>) selectedActivityServicesDetails.stream().filter(filter)
					.collect(Collectors.toList());

			// if no search found by Name, finding with Address.
			if (filteredServiceDetails.isEmpty()) {
				filter = map -> ((String) map.get("Address")).toLowerCase().contains(searchInput.toLowerCase());
				filteredServiceDetails = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
						.filter(filter).collect(Collectors.toList());
			}
			if (!filteredServiceDetails.isEmpty())
				filteredServiceDetails = transformFilteredServiceDetails(filteredServiceDetails, activityType);
		}

		// if search is found either by name or address, then transforming it
		// by adding new field 'activityType' in each serviceDetail along with sorting
		// and cityMap creation
		if (filteredServiceDetails != null) {
			filteredServiceDetails.sort((o1, o2) -> ((Long) o2.get("Offer")).compareTo((Long) o1.get("Offer")));
			cityMap = filteredServiceDetails.stream()
					.flatMap(map -> map.entrySet().stream().filter(item -> item.getKey().equals("City")))
					.collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.counting()));
		}

		ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(filteredServiceDetails);
		System.out.println(json);
		request.setAttribute("activityType", (activityType == null || activityType == "") ? "" : activityType);
		request.setAttribute("searchText", (searchInput == null || searchInput == "") ? "" : searchInput);
		request.setAttribute("cityMap", cityMap);
		request.setAttribute("searchedservices", filteredServiceDetails);
		response.setRenderParameter("mvcPath", "/listing.jsp");
	}

	private List<Map<String, Object>> transformFilteredServiceDetails(List<Map<String, Object>> filteredServiceDetails,
			String activityType) {
		List<Map<String, Object>> transformed = new ArrayList<>();
		filteredServiceDetails.stream().map(map -> {
			Set<String> keys = map.keySet();
			Map<String, Object> newMap = new HashMap<>();
			for (String key : keys) {
				if (key.equals("Image")) {
					newMap.put("Image", "/assets/images/" + activityType + "/" + (String) map.get("Image"));
				} else {
					newMap.put(key, map.get(key));
				}
			}
			newMap.put("activityTpye", activityType);
			return newMap;
		}).forEach(map -> transformed.add(map));

		return transformed;
	}

	@SuppressWarnings({ "unchecked", "deprecation" })
	@ProcessAction(name = "getSelectedServiceDetails")
	public void getServiceDetails(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		String activityType = ParamUtil.getString(request, "activityType");
		Long serviceId = ParamUtil.getLong(request, "serviceId");
		List<Map<String, Object>> selectedServiceDetails = null;

		List<Map<String, Object>> selectedActivityServicesDetails = null;
		if (activityType.equals("Salon"))
			selectedActivityServicesDetails = SalonDetails;
		else if (activityType.equals("Hospital"))
			selectedActivityServicesDetails = HospitalDetails;
		else if (activityType.equals("Grocery"))
			selectedActivityServicesDetails = GroceryDetails;

		selectedServiceDetails = (List<Map<String, Object>>) selectedActivityServicesDetails.stream()
				.filter(map -> ((Long) map.get("Id")).equals(serviceId)).collect(Collectors.toList());
		selectedServiceDetails = transformFilteredServiceDetails(selectedServiceDetails, activityType);

		request.setAttribute("activityType", (activityType == null || activityType == "") ? "" : activityType);
		request.setAttribute("selectedServiceDetails", selectedServiceDetails.get(0));
		response.setRenderParameter("mvcPath", "/detail-page.jsp");
	}

	@SuppressWarnings("unchecked")
	private static void loadDataOnStartup(RenderRequest request) {
		System.out.println("loading data .....");
		JSONParser parser = new JSONParser();
		JSONObject suggestionsObject = null;
		Object obj;
		try {

			// initialize Salon data
			InputStream stream = request.getPortletContext().getResourceAsStream("/json/Salon.json");
			BufferedReader bufRead = new BufferedReader(new InputStreamReader(stream));
			StringBuilder builder = new StringBuilder();
			String line = null;
			while ((line = bufRead.readLine()) != null) {
				builder.append(line).append("\n");
			}
			obj = parser.parse(builder.toString());
			suggestionsObject = (JSONObject) obj;
			Map<String, Object> map = toMap(suggestionsObject);
			SalonDetails = (List<Map<String, Object>>) map.get("SalonDetails");
			AllServiceDetails.addAll(SalonDetails);
			activitiesList.add("Salon");
			AllServiceDetailsActivityWise.add(map);

			// initialize Hospital data
			stream = request.getPortletContext().getResourceAsStream("/json/Hospital.json");
			bufRead = new BufferedReader(new InputStreamReader(stream));
			builder = new StringBuilder();
			line = null;
			while ((line = bufRead.readLine()) != null) {
				builder.append(line).append("\n");
			}
			obj = parser.parse(builder.toString());
			suggestionsObject = (JSONObject) obj;
			map = toMap(suggestionsObject);
			HospitalDetails = (List<Map<String, Object>>) map.get("HospitalDetails");
			AllServiceDetails.addAll(HospitalDetails);
			activitiesList.add("Hospital");
			AllServiceDetailsActivityWise.add(map);

			// initialize Grocery data
			stream = request.getPortletContext().getResourceAsStream("/json/Grocery.json");
			bufRead = new BufferedReader(new InputStreamReader(stream));
			builder = new StringBuilder();
			line = null;
			while ((line = bufRead.readLine()) != null) {
				builder.append(line).append("\n");
			}
			obj = parser.parse(builder.toString());
			suggestionsObject = (JSONObject) obj;
			map = toMap(suggestionsObject);
			GroceryDetails = (List<Map<String, Object>>) map.get("KiranaStoreDetails");
			AllServiceDetails.addAll(GroceryDetails);
			activitiesList.add("Grocery");
			AllServiceDetailsActivityWise.add(map);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Map<String, Object> toMap(JSONObject jsonobj) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Set<String> keySet = jsonobj.keySet();
		for (String key : keySet) {
			Object value = jsonobj.get(key);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	public static List<Object> toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.size(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

}
