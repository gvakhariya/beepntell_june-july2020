﻿<%@ include file="/init.jsp" %>

<portlet:defineObjects />

<portlet:resourceURL var="fetchSearchSuggestionsURL" id="fetchSearchSuggestions">
</portlet:resourceURL>
<portlet:resourceURL var="getOffersTrendingRecentReviewsDataForActivityURL" id="getOffersTrendingRecentReviewsDataForActivity">
</portlet:resourceURL>
<portlet:actionURL name="listingServices" var="listingURL"/>
<portlet:actionURL name="getSelectedServiceDetails" var="getSelectedServiceDetailsURL"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Beepntell</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
    <link rel="icon" type="image/png" href="<%=request.getContextPath()%>/assets/images/favicon.png">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.mCustomScrollbar.css"> 
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">      
</head>
<body>
    <div class="d-flex">
        <div class="left-side-bar">
            <div class="d-none-d cmd-d-block close-p">
                <a href="javascript:void(0)" class="for-sprite-call close-menu"></a>
            </div>
            <div class="mCustomScrollbar scroll-height-side-bar" data-mcs-theme="minimal-dark">
                <div class="filtrar-overlay"></div>
                <ul id="menu">
                    <li onclick="changeMenu(1,'Society_visit_request','Search for a Society Visit Request')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-society-req"></i>
                            <span>Society Visit Request</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(2,'Grocery','Search for a Grocery','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                           '<%=request.getContextPath()%>','<%=getSelectedServiceDetailsURL%>')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-grocery"></i>
                            <span>Grocery</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(3,'Hospital','Search for a Hospital','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                          '<%=request.getContextPath()%>','<%=getSelectedServiceDetailsURL%>')">
                        <a href="javascript:void(0)" title="">
                            <img style="margin-right: 15px;" src="<%=request.getContextPath()%>/assets/images/hospital.svg">
                            <span>Hospital</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(4,'Salon','Search for a Salon','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                           '<%=request.getContextPath()%>', '<%=getSelectedServiceDetailsURL%>')">
                        <a href="javascript:void(0)" title="">
							<img style="margin-right: 15px;" src="<%=request.getContextPath()%>/assets/images/hair.svg">
                            <span>Salon</span>
                        </a>
                    </li>
                     <li onclick="changeMenu(5,'Cab_booking','Search for a Cab Booking')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-cab-booking"></i>
                            <span>Cab Booking</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(6,'Book_car_service','Search for a Book Car Service')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-car-service"></i>
                            <span>Car Services Booking</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(7,'News','Search for News')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-news"></i>
                            <span>News</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(8,'Rewards','Search for Rewards')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-rewards"></i>
                            <span>Rewards</span>
                        </a>
                    </li>
                </ul>
            	<ul id="product">
                    <li onclick="changeMenu(1,'Society_visit_request','Search for a Society Visit Request')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-society-req"></i>
                            <span>Beep N Visit</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(2,'Grocery','Search for a Grocery','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                           '<%=request.getContextPath()%>','<%=getSelectedServiceDetailsURL%>')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-grocery"></i>
                            <span>Beep N Buy</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(3,'Hospital','Search for a Hospital','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                          '<%=request.getContextPath()%>','<%=getSelectedServiceDetailsURL%>')">
                        <a href="javascript:void(0)" title="">
                            <img style="margin-right: 15px;" src="<%=request.getContextPath()%>/assets/images/hospital.svg">
                            <span>Beep N Concierge</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(4,'Salon','Search for a Salon','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                           '<%=request.getContextPath()%>', '<%=getSelectedServiceDetailsURL%>')">
                        <a href="javascript:void(0)" title="">
							<img style="margin-right: 15px;" src="<%=request.getContextPath()%>/assets/images/hair.svg">
                            <span>Beep N Stay</span>
                        </a>
                    </li>
                     <li onclick="changeMenu(5,'Cab_booking','Search for a Cab Booking')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-cab-booking"></i>
                            <span>Beep N Ride</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(6,'Book_car_service','Search for a Book Car Service')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-car-service"></i>
                            <span>Beep N Book</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(7,'News','Search for News')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-news"></i>
                            <span>Beep N Next</span>
                        </a>
                    </li>
                    <li onclick="changeMenu(8,'Rewards','Search for Rewards')">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-rewards"></i>
                            <span>Beep N Say</span>
                        </a>
                    </li>
                </ul>
            
            </div>
        </div>
        <div class="right-side-bar">
            <main>
                <header>
                    <div class="menu-icon-mob d-none-d cmd-d-block">
                        <a href="javascript:void(0)" class="for-sprite-call ic-mob-icon"></a>
                    </div>
                    <div class="d-flex justify-content-end">
                        <div class="login">
                            <a href="javascript:void(0)" class="d-flex align-items-center">
                                <i class="for-sprite-call ic-user"></i>
                                <span>Log In</span>
                            </a>
                        </div>
                        <div class="category">
                            <a onclick="productsToggle()" href="javascript:void(0)" class="for-sprite-call ic-category"></a>
                            <div id="products-div">
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Visit</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Stay</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Buy</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Concierge</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Book</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Next</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Say</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Ride</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="search-part">

                    <div style="margin: 50px 0;" class="d-none-d csd-d-block mob-cat-div">
                        <div class="owl-carousel owl-theme cat-mob-slider-slider">
                            <div class="item">
                                <a href="javascript:void(0)" title="">                                    
                                    <span>Society Visit Request</span>
                                </a>                              
                            </div>
                            <div class="item" id="act2" onclick="changeMenu(2,'Grocery','Search for a Grocery','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                           '<%=request.getContextPath()%>','<%=getSelectedServiceDetailsURL%>')">
                                <a href="javascript:void(0)" title="">
                                    <span>Grocery</span>
                                </a>                                
                            </div>
                             <div class="item" id="act3" onclick="changeMenu(3,'Hospital','Search for a Hospital','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                          '<%=request.getContextPath()%>','<%=getSelectedServiceDetailsURL%>')">
                                <a href="javascript:void(0)" title="">
                                    <span>Hospital</span>
                                </a>                                
                            </div>
                             <div class="item" id="act4" onclick="changeMenu(4,'Salon','Search for a Salon','<%=getOffersTrendingRecentReviewsDataForActivityURL%>',
                                           '<%=request.getContextPath()%>', '<%=getSelectedServiceDetailsURL%>')">
                                <a href="javascript:void(0)" title="">
                                    <span>Salon</span>
                                </a>                                
                            </div>
                             <div class="item" >
                                <a href="javascript:void(0)" title="">
                                    <span>Cab Booking</span>
                                </a>                                
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">                                    
                                    <span>Car Services Booking</span>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="text-center brand-name">
                        <a href="javascript:void(0)" class=""><img src="<%=request.getContextPath()%>/assets/images/logo.svg" alt="Beepntell" class="img-fluid"></a>
                    </div>
                    <div class="search-suggestions open-suggestions position-relative">
                        <div class="input-group ">
                            <input type="text" id="suggestion-input" oninput="takeInput('<%=fetchSearchSuggestionsURL%>','<%=listingURL%>')" class="form-control add-suggestions" placeholder="Search for a Service/Product grocery store, saloon, hospital, dance classes etc ">
                            <a href="javascript:void(0)" class="voice-search for-sprite-call"></a>
                            <div class="input-group-append">
                              <button id="serach-button" class="btn btn-secondary" type="button" onclick="gotoListingPage('')">
                                <i class="ic-search for-sprite-call"></i>
                              </button>
                            </div>
                        </div>  
                          <div id ="suggestions-results-div"></div>
                        <!-- <ul class="suggestions-results font-weight-bold"> -->
                            <!-- <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Om Shivay Kirana Store</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Sumer Chand Tara Chand</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Big Mart Retail Solutions Pvt Ltd</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Easy Day Express </a>
                            </li> -->
                        <!-- </ul> -->
                      <div class="text-center bhasha">
                            <p><strong>Beepntell offered in:</strong> <a href="javascript:void(0)">हिन्दी</a> <a href="javascript:void(0)"> বাংলা</a> <a href="javascript:void(0)">తెలుగు</a> <a href="javascript:void(0)">मराठी</a> <a href="javascript:void(0)">தமிழ்</a> <a href="javascript:void(0)">ગુજરાતી</a> <a href="javascript:void(0)">ಕನ್ನಡ</a></p>
                        </div>                        
                    </div>
                </section>
                <section class="offers-reviews">
                    <div class="d-flex justify-content-between clg-flex-wrap">
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Offers &amp; Discount</h2>
                            </div>
                            <div  id="topOffers_div" class="owl-carousel owl-theme top-review-slider">
                                <c:forEach items="${topOffersServices}" var="topOffersService">
                                <div class="item" onClick="gotoDetailPage('<%= getSelectedServiceDetailsURL%>',
                                            ${topOffersService.Id},'${topOffersService.activityTpye}')">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                   <img src="<%=request.getContextPath()%>${topOffersService.Image}" alt="" class="object-fit-cover">
                       
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">${topOffersService.Name}</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>${topOffersService.Rating}</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>${topOffersService.Reviews} reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>${topOffersService.Offer}%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                   </div>
                                </div> 
                                </c:forEach>
                            </div>                            
                        </div>
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Recent Reviews</h2>
                            </div>
                            <div id="topRecentReviews_div" class="owl-carousel owl-theme top-review-slider">
                            <c:forEach items="${topRecentReviewsServices}" var="topRecentReviewsService">
                            <div class="item" onClick="gotoDetailPage('<%= getSelectedServiceDetailsURL%>',
                                            ${topRecentReviewsService.Id},'${topRecentReviewsService.activityTpye}')">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                  <img src="<%=request.getContextPath()%>${topRecentReviewsService.Image}" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                             <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                       </div>
                                                        <div class="user-name">
                                            		     <p>${topRecentReviewsService.ReviewComments[0].Reviewer}</p>
                                                                  <fmt:parseDate value="${topRecentReviewsService.ReviewComments[0].ReviewDate}" pattern="yyyy-MM-dd\'T\'HH:mm:ss.SSS'Z'" var="formattedReviewDate" parseLocale="en_GB"/>
	                                                              <fmt:formatDate value="${formattedReviewDate}" pattern="MMM dd, yyyy HH:mm a" var="finalFormattedDate"/> 
                              
                                                            <p>${finalFormattedDate}</p>
                              				            </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>${topRecentReviewsService.ReviewComments[0].ReviewRating} Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                             <h2 class="font-weight-bold">${topRecentReviewsService.Name}</h2>
                                                <p>${topRecentReviewsService.ReviewComments[0].Comment}</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                              </c:forEach> 
                            </div>                            
                        </div>
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Trending Reviews</h2>
                            </div>
                            <div id="topTrendings_div" class="owl-carousel owl-theme top-review-slider">
                          	<c:forEach items="${topTrendingReviewsServices}" var="topTrendingReviewsService">
                                <div class="item" onClick="gotoDetailPage('<%= getSelectedServiceDetailsURL%>',
                                            ${topTrendingReviewsService.Id},'${topTrendingReviewsService.activityTpye}')">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                   <img src="<%=request.getContextPath()%>${topTrendingReviewsService.Image}" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>${topTrendingReviewsService.ReviewComments[0].Reviewer}</p>
                                                                  <fmt:parseDate value="${topTrendingReviewsService.ReviewComments[0].ReviewDate}" pattern="yyyy-MM-dd\'T\'HH:mm:ss.SSS'Z'" var="formattedReviewDate" parseLocale="en_GB"/>
	                                                              <fmt:formatDate value="${formattedReviewDate}" pattern="MMM dd, yyyy HH:mm a" var="finalFormattedDate"/> 
                              
                                                            <p>${finalFormattedDate}</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>${topTrendingReviewsService.ReviewComments[0].ReviewRating} Star</p>
                                                        <p>${topTrendingReviewsService.ReviewComments[0].Like} likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">${topTrendingReviewsService.Name}</h2>
                                                <p>${topTrendingReviewsService.ReviewComments[0].Comment}</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                                </c:forEach> 
                            </div>                            
                        </div>
                    </div>
                </section>
                 <div id="chatbot-button" class="chat-bot-button-container">
                    <div onclick="openChatbot()" class="chat-bot-button">
                        <button>
                            <img src="<%=request.getContextPath()%>/assets/images/comment.svg">
                        </button>
                    </div>
                </div>
                <div id="chatbot" class="chat-bot-container">
                    <div class="chat-bot">
                        <div class="chatbot-cross">
                            <p class="chatbot-cross-pointer" onclick="openChatbot()">x</p>
                        </div>
                        <div>
                            <p class="chatbot-heading"> Welcome to Beepntell </p>
                            <div class="flex space-between padding-20">
                                <div class="small-icon">
                                    <img src="<%=request.getContextPath()%>/assets/images/comment-small.svg">
                                </div>
                                <div class="activity-selector">
                                    <p>Hi, Please select from below activity or enter your query</p>
                                </div>
                            </div>
                            <div class="flex space-between activitys">
                                <div class="activity1-button">
                                    <p>Book Appoinment</p>
                                </div>
                                <div class="activity2-button">
                                    <p>Grocery</p>
                                </div>
                                <div class="activity1-button">
                                    <p>Book Services</p>
                                </div>
                                <div class="activity2-button">
                                    <p>Cab Booking</p>
                                </div>
                            </div>
                        </div>
                        <div class="chat-input-div">
                            <input class="chat-input" type="text" placeholder="Enter your query">
                            <img style="margin-right: 10px;" src="<%=request.getContextPath()%>/assets/images/send.svg">
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                <div class="d-flex justify-content-between">
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Promote</a></li>
                        <li><a href="javascript:void(0)">Business Page</a></li>
                        <li><a href="javascript:void(0)">Rewards/Offer</a></li>
                        <li><a href="javascript:void(0)">About</a></li>
                    </ul>
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Privacy</a></li>
                        <li><a href="javascript:void(0)">Terms</a></li>
                        <li><a href="javascript:void(0)">Setting</a></li>
                    </ul>
                </div>
            </footer>
        </div>
    </div>    
    <script src="<%=request.getContextPath()%>/assets/js/jquery-3.js"></script> 
    <script src="<%=request.getContextPath()%>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/owl.carousel.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/index.js"></script> 
    <script src="<%=request.getContextPath()%>/assets/js/myFunctions.js"></script>  
       
    
</body>
</html>