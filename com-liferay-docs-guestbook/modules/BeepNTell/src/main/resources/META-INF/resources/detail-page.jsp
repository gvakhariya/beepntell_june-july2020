<%@ include file="/init.jsp" %>

<portlet:defineObjects />

<portlet:resourceURL var="filterReviewsForSelectedServiceURL" id="filterReviewsForSelectedService">
</portlet:resourceURL>


<!DOCTYPE html>
<html lang="en">
<head>     
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Beepntell</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
    <link rel="icon" type="image/png" href="<%=request.getContextPath()%>/assets/images/favicon.png">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.mCustomScrollbar.css"> 
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">      
</head>
<body>
    <header class="inner-page-header">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="brand-logo">
                    <a href="javascript:void(0)"><img src="<%=request.getContextPath()%>/assets/images/logo.svg" alt="" class="img-fluid"></a>
                </div>
                <div class="d-flex">
                    <div class="login">
                        <a href="javascript:void(0)" class="d-flex align-items-center">
                            <i class="for-sprite-call ic-user"></i>
                            <span>Log In</span>
                        </a>
                    </div>
                    <div class="category">
                     <a onclick="productsToggle()" href="javascript:void(0)" class="for-sprite-call ic-category"></a>
                            <div id="products-div">
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Visit</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Stay</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Buy</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Concierge</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Book</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Next</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Say</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Ride</p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>        
    </header>
    <main>
        <div class="back">
            <div class="container">
                <a href="javascript:void(0)" class="for-before-sprite ic-back-arrow to-home">${selectedServiceDetails.Name}</a>
            </div>            
        </div>
        <section class="store-info">
            <div class="container">
                <div class="store-details">
                    <div class="d-flex justify-content-between align-items-center csd-flex-wrap">
                        <div class="store-full-de">
                            <h1 class="font-weight-bold">${selectedServiceDetails.Name}</h1>
                            <div class="d-flex star-r align-items-center cxsd-flex-wrap">
                                <p>${selectedServiceDetails.Rating}</p>
                                <div class="star-icon">
                                 <c:forEach begin="1" end="${selectedServiceDetails.Rating}" var="i">
                                        <span class="star-img fill-star"></span>
                                 </c:forEach>
                                 <c:forEach begin="${selectedServiceDetails.Rating}" end="4" var="j">
                                        <span class="star-img blank-star"></span>
                                 </c:forEach>
                                </div>
                                <span class="total-review font-weight-bold">${selectedServiceDetails.Reviews} reviews</span>
                            </div>
                            <div class="d-flex justify-content-between align-items-end clg-flex-wrap">
                                <address class="store-address">
                                    <p class="ic-phone ic-identy-icon for-before-sprite">${selectedServiceDetails.Phone}</p>
                                    <p class="ic-address ic-identy-icon for-before-sprite">${selectedServiceDetails.Address}</p>
                                </address>
                               <c:choose>
	                                <c:when test="${activityType eq 'Grocery'}">
	                                	<a href="javascript:void(0)" class="btn-order-now">Order Now</a>
	                                </c:when>
	                                 <c:otherwise>
	                                	<a href="javascript:void(0)" class="btn-order-now">Book Appointment</a>
	                                </c:otherwise>
	                            </c:choose>
                                
                            </div>
                        </div>
                        <div class="store-img-de">
                        			<img src="<%=request.getContextPath()%>${selectedServiceDetails.Image}" alt="" class="object-fit-cover">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="reviews-section">
            <div class="container">
                <div class="in-detail-review">
                    <div class="d-flex justify-content-between align-items-center title-select">
                        <h3 class="font-weight-bold">${selectedServiceDetails.Name} Reviews</h3>
                        <div class="select-here position-relative for-before-sprite csd-d-none">
                           
                            <select id="sorting-select" name="" class="custom-select"
                             onchange="applyReviewsFilterOnDetailPage('<%=request.getContextPath()%>',
                             '<%=filterReviewsForSelectedServiceURL%>',
                            '${selectedServiceDetails.activityTpye}','${selectedServiceDetails.Id}')">
                                <option selected>All Reviews</option>
                                <option  value=1>Most HelpFul</option>
                                <option value=2>with 4 star</option>
                            </select>
                            
                        </div>
                    </div>
                    <div id="ratings-commnents-div">
                    <c:forEach items="${selectedServiceDetails.ReviewComments}" var="reviewDetails">
                    <div class="indiv-rating-review">
                        <div class="d-flex justify-content-between csd-flex-wrap">
                            <div class="user-info text-center">
                                <div class="user-photoes">
                                    <img src="<%=request.getContextPath()%>/assets/images/user_pic_a.png" alt="" class="object-fit-cover">
                                </div>
                                <div class="user-name">
                                    <p>${reviewDetails.Reviewer}</p>
                         <!--            <p>Shiv Kunj, Sant Nagar, India</p>
                          -->       </div>
                            </div>
                            <div class="user-comments">
                            <!--     <h2>Great travel agency</h2>
                             -->    <div class="d-flex star-r align-items-center cxsd-flex-wrap">
                                    <p>${reviewDetails.ReviewRating}</p>
                                    <div class="star-icon">
                                    <c:forEach begin="1" end="${reviewDetails.ReviewRating}" var="i">
                                        <span class="star-img fill-star"></span>
                                    </c:forEach>
                                     <c:forEach begin="${reviewDetails.ReviewRating}" end="4" var="j">
                                        <span class="star-img blank-star"></span>
                                        </c:forEach>
                                    </div>
                                    <span class="date-time">
                                    <fmt:parseDate value="${reviewDetails.ReviewDate}" pattern="yyyy-MM-dd\'T\'HH:mm:ss.SSS'Z'" var="formattedReviewDate" parseLocale="en_GB"/>
	                                <fmt:formatDate value="${formattedReviewDate}" pattern="MMM dd, yyyy HH:mm a" /> 
                                    </span>
                                </div>
                                <div class="user-exp">
                                    <p>${reviewDetails.Comment}</p>
                                </div>
                                <div class="like-count">
                                    <p>
                                        <i class="for-sprite-call ic-like"></i> ${reviewDetails.Like} Likes
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </c:forEach>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="d-flex justify-content-between">
            <ul class="footer-links">
                <li><a href="javascript:void(0)">Promote</a></li>
                <li><a href="javascript:void(0)">Business Page</a></li>
                <li><a href="javascript:void(0)">Rewards/Offer</a></li>
                <li><a href="javascript:void(0)">About</a></li>
            </ul>
            <ul class="footer-links">
                <li><a href="javascript:void(0)">Privacy</a></li>
                <li><a href="javascript:void(0)">Terms</a></li>
                <li><a href="javascript:void(0)">Setting</a></li>
            </ul>
        </div>
    </footer>    
    <script src="<%=request.getContextPath()%>/assets/js/jquery-3.js"></script> 
    <script src="<%=request.getContextPath()%>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/owl.carousel.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/index.js"></script>  
    <script src="<%=request.getContextPath()%>/assets/js/myFunctions.js"></script>    
    
</body>
</html>