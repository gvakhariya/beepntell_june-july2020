<%@ include file="/init.jsp" %>

<portlet:defineObjects />


<portlet:actionURL name="fetchSearchSuggestionsForAllActivities" var="fetchSearchSuggestionsForAllActivitiesURL"/>

<aui:form action="<%=fetchSearchSuggestionsForAllActivitiesURL%>" method="POST">
<aui:input type="submit" value="Fetch suggestion" name="fetchSuggestions"></aui:input>
</aui:form>

<table>    
      
        <portlet:actionURL name="fetchSearchSuggestionsForSelectedActivity" var="fetchSearchSuggestionsForSelectedActivityURL">
            <portlet:param name="activity" value="1"/>
        </portlet:actionURL>
        <tr>
        <td class="text-center" style="width: 50px">
                <a href="<%=fetchSearchSuggestionsForSelectedActivityURL%>">
                Activity - 1</a>
            </td> 
        </tr>
        
      <portlet:actionURL name="fetchSearchSuggestionsForSelectedActivity" var="fetchSearchSuggestionsForSelectedActivityURL">
            <portlet:param name="activity" value="2"/>
        </portlet:actionURL>
        <tr>
        <td class="text-center" style="width: 50px">
                <a href="<%=fetchSearchSuggestionsForSelectedActivityURL%>">
                Activity - 2</a>
            </td> 
        </tr>

</table>