var menu = "default";
var selectedActivityType = "";
var listingServicesActionURL = "";
var ratingFilterValues = [];
var cityData = [];
var cityCheckBoxData = [];
var cityFinalFilterInput = [];

function changeMenu(value,activityType, placeholder,getOffersTrendingRecentReviewsDataForActivityURL,contextPath,getSelectedServiceDetailsURL) {
	  if (menu != "default") {
		  if($(window).width()>767){
		      $(`ul#menu  li:nth-child(${menu})`).removeClass("active-menu");
		    } else {
		      $(`#act${menu}.item`).css("color", "#000");
		    $(`#act${menu}.item`).css("background", "#e5e5e5");
		    }
//	    $(`ul#menu  li:nth-child(${menu})`).removeClass("active-menu");
	  }
	  menu = value;
	  selectedActivityType = activityType;
	  $("#suggestion-input").attr("placeholder", placeholder);
	  $("#suggestion-input").val("");
	  takeInput();
//	  $(`ul#menu li:nth-child(${value})`).addClass("active-menu");
	  if($(window).width()>767){
		    $(`ul#menu li:nth-child(${value})`).addClass("active-menu");
		  } else {
		    $(`#act${menu}.item`).css("color", "#fff");
		    $(`#act${menu}.item`).css("background", "#000");
		    }
	  
	  loadOffersTrendingRecentsData(getOffersTrendingRecentReviewsDataForActivityURL,activityType,contextPath,getSelectedServiceDetailsURL);
	 
}

function loadOffersTrendingRecentsData(getOffersTrendingRecentReviewsDataForActivityURL,activityType,contextPath,getSelectedServiceDetailsURL){
	
	$.ajax({
		url : getOffersTrendingRecentReviewsDataForActivityURL,
		type : 'post',
		datatype : 'json',
		data : {
			"activityType" : activityType
		},
		success : function(data) {
			console.log(data);
			responseData = $.parseJSON(data);
			 showTopOfferServicesData(responseData,activityType,contextPath,getSelectedServiceDetailsURL); 
			 showTopRecentReviewsServicesData(responseData,activityType,contextPath,getSelectedServiceDetailsURL);
			 showTopTrendingServicesData(responseData,activityType,contextPath,getSelectedServiceDetailsURL);
			
			// $('#topOffers_div').html('');
			// $('#topOffers_div').addClass("owl-carousel owl-theme top-review-slider");
			// $('#topOffers_div').html(htmlDataForTopOfferServices);
			 
			// $('#topTrendings_div').html('');
			// $('#topTrendings_div').addClass("owl-carousel owl-theme top-review-slider");
			// $('#topTrendings_div').html(htmlDataForTopTrendingServices);
		}
	});
}

function showTopOfferServicesData(data,activityType,contextPath,getSelectedServiceDetailsURL){
	
	 let htmlDataForTopOfferServices = '';  
	 data.topOffersServices.forEach(element => {
		 htmlDataForTopOfferServices += `<div class="item" onClick="gotoDetailPage('${getSelectedServiceDetailsURL}',${element.Id},'${activityType}')">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                   <img src="${contextPath}/assets/images/${activityType}/${element.Image}" alt="" class="object-fit-cover">
                       
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">${element.Name}</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>${element.Rating}</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>${element.Reviews} reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>${element.Offer}%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                       `
	  });
//	 
//	 return htmlDataForTopOfferServices;
	 $('#topOffers_div.owl-carousel').trigger('replace.owl.carousel', 
			 htmlDataForTopOfferServices
			  ).trigger('refresh.owl.carousel');

}

function showTopRecentReviewsServicesData(data,activityType,contextPath,getSelectedServiceDetailsURL){
	 let htmlDataForTopRecentReviewsServices = '';  
	 data.topRecentReviewsServices.forEach(element => {
		  var reviewDate = JSON.stringify(element.ReviewComments[0].ReviewDate);
		  reviewDate = new Date(JSON.parse(reviewDate));
		 
		 htmlDataForTopRecentReviewsServices += ` <div class="item" onClick="gotoDetailPage('${getSelectedServiceDetailsURL}',${element.Id},'${activityType}')">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                   <img src="${contextPath}/assets/images/${activityType}/${element.Image}" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="${contextPath}/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>${element.ReviewComments[0].Reviewer}</p>
                                                            <p>
                                                                   <span class="date-time">${reviewDate.toString().substr(4,6) + ',' + reviewDate.toString().substr(10,6) +  reviewDate.toString().substr(16,5) + ' ' + (reviewDate.getHours() > 12 ? 'PM' : 'AM')}</span>
                                                           </p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>${element.ReviewComments[0].ReviewRating} Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">${element.Name}</h2>
                                                <p>${element.ReviewComments[0].Comment}</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                               `; 
	  });
	 
//	 return htmlDataForTopRecentReviewsServices;
	 $('#topRecentReviews_div.owl-carousel').trigger('replace.owl.carousel', 
			 htmlDataForTopRecentReviewsServices
			  ).trigger('refresh.owl.carousel');
}

function showTopTrendingServicesData(data,activityType,contextPath,getSelectedServiceDetailsURL){
	 let htmlDataForTopTrendingServices = '';  
	 data.topTrendingReviewsServices.forEach(element => {
		  var reviewDate = JSON.stringify(element.ReviewComments[0].ReviewDate);
		  reviewDate = new Date(JSON.parse(reviewDate));
		 
		 htmlDataForTopTrendingServices += ` <div class="item" onClick="gotoDetailPage('${getSelectedServiceDetailsURL}',${element.Id},'${activityType}')">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                   <img src="${contextPath}/assets/images/${activityType}/${element.Image}" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="${contextPath}/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>${element.ReviewComments[0].Reviewer}</p>
                                                            <p>
                                                                   <span class="date-time">${reviewDate.toString().substr(4,6) + ',' + reviewDate.toString().substr(10,6) +  reviewDate.toString().substr(16,5) + ' ' + (reviewDate.getHours() > 12 ? 'PM' : 'AM')}</span>
                                                           </p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>${element.ReviewComments[0].ReviewRating} Star</p>
                                                        <p>${element.ReviewComments[0].Like} likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">${element.Name}</h2>
                                                <p>${element.ReviewComments[0].Comment}</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                      `
	  });
	 
//	 return htmlDataForTopTrendingServices;
	 $('#topTrendings_div.owl-carousel').trigger('replace.owl.carousel', 
			 htmlDataForTopTrendingServices
			  ).trigger('refresh.owl.carousel');
	 
}

function takeInput(fetchSearchSuggestionsResourceURL,listingActionURL) {
	
	let searchText = $("#suggestion-input").val();
	if (searchText.length > 0) {
		    listingServicesActionURL = listingActionURL;
			let data = getSearchSuggestions(fetchSearchSuggestionsResourceURL,searchText);
    } else {
		    $(".open-suggestions").removeClass("suggestions-des");
		    $("#serach-button").removeClass("suggestions-des");
    }
}

function getSearchSuggestions(fetchSearchSuggestionsResourceURL,searchText) {
	var responseData;
	$.ajax({
		url : fetchSearchSuggestionsResourceURL,
		type : 'post',
		datatype : 'json',
		data : {
			"activityType" : selectedActivityType,
			"searchText" : searchText
		},
		success : function(data) {
			responseData = $.parseJSON(data);
		    showData(responseData, searchText); 
		}
	});
}


function showData(responseData, searchText) {
	  $(".open-suggestions").addClass("suggestions-des");
	  $("#serach-button").addClass("suggestions-des");
	  let htmlData = `<ul class="suggestions-results font-weight-bold">`;
	  responseData.forEach((element) => {
	    let sendElement = JSON.stringify(element);
	    htmlData += `<li onClick="gotoListingPage(escape('${element}'))"><a href="javascript:void(0)" class="for-before-sprite">${element}</a></li>`;
	    });
	  htmlData += `</ul>`;
	  $("#suggestions-results-div").html(htmlData);
}


function gotoListingPage(searchText) {
	if(searchText == "" || searchText == null){
		searchText = $('#suggestion-input').val();
	}
	location.href = listingServicesActionURL + "&activityType="+selectedActivityType+"&searchText=" + searchText;
}

function applyFilter(contextPath,filterListedServicesURL,i,activity,searchInput,getSelectedServiceDetailsURL,isRatingCheckBoxFilter,isCityNameFilter, isCityCheckBoxFilter){
	if(isRatingCheckBoxFilter) {
		if($("#ratingCheckBox"+i).prop("checked")){
			 ratingFilterValues.push($("#ratingCheckBox"+i).val());
			  }
		 else{
			 ratingFilterValues.splice(ratingFilterValues.indexOf($("#ratingCheckBox"+i).val()), 1);
		 }
	}
	
	if(isCityNameFilter) {
		cityFinalFilterInput = [];
		cityCheckBoxData = [];
		var searchTxt = $("#citySearchText").val();
		if(searchTxt.length > 0) {
		    cityFinalFilterInput.push(searchTxt);
		}
	}

	if(isCityCheckBoxFilter) {
		 if($("#cityCheckBox"+i).prop("checked")){
			 cityCheckBoxData.push($("#cityCheckBox"+i).val());
			  }
		 else{
			 cityCheckBoxData.splice(ratingFilterValues.indexOf($("#cityCheckBox"+i).val()), 1);
		 }
	}
  
  getFilteredList(contextPath,filterListedServicesURL,activity,searchInput,getSelectedServiceDetailsURL,isCityNameFilter, isCityCheckBoxFilter);
}

function getFilteredList(contextPath,filterListedServicesURL,activity,searchInput,getSelectedServiceDetailsURL,isCityNameFilter, isCityCheckBoxFilter){
	$.ajax({
		url : filterListedServicesURL,
		type : 'post',
		datatype : 'json',
		data : {
			"activityType" : activity,
			"searchText" : searchInput,
			"ratingInput" : ratingFilterValues,
			"cityInput" : cityFinalFilterInput
		},
		success : function(data) {
			var responseData = $.parseJSON(data);
			if(responseData.filteredSearchServices.length > 0){
					var htmlDataForListing = getRenderedListingServices(contextPath,responseData,activity,getSelectedServiceDetailsURL);
					var htmlDataForCityList = getRenderedCityList(contextPath,responseData,activity,searchInput,filterListedServicesURL,getSelectedServiceDetailsURL);
					$("#list-details").html("");
					$("#list-details").html(htmlDataForListing);
					$("#cityList").html("");
					$("#cityList").html(htmlDataForCityList);
					
			}
			
			else {
				    $("#list-details").html('<h3 class="text-align-center">No Services Found To Show You</h3>');
				    $("#cityList").html('<h3 class="text-align-center">No cities available</h3>');
			  }
		}
	});
}

function getRenderedCityList(contextPath,responseData,activity,searchInput,filterListedServicesURL,getSelectedServiceDetailsURL){
	  let htmlDataForCityList ='';
	  let cityData = responseData.cityMap;
	  var index=0;
	  for (var key in cityData) {
		  if(cityCheckBoxData.includes(key)){
		  htmlDataForCityList += `
			    <div class="form-group">
			                                    <div class="custom-control custom-checkbox">
			                                       <input type="checkbox" class="custom-control-input" id="cityCheckBox${index}" value="${key}" 
			                                       onchange="applyFilter('${contextPath}',
                                                                  '${filterListedServicesURL}','${index}','${activity}','${searchInput}',
                                                                  '${getSelectedServiceDetailsURL}',false,false,true)" checked>
			                                        <label class="custom-control-label" for="cityCheckBox${index}">${key} (${cityData[key]})</label>
			                                    </div>
			                                </div>
			                                `;
		  } else {
			  htmlDataForCityList += `
				    <div class="form-group">
				                                    <div class="custom-control custom-checkbox">
				                                       <input type="checkbox" class="custom-control-input" id="cityCheckBox${index}" value="${key}" 
				                                       onchange="applyFilter('${contextPath}',
	                                                                  '${filterListedServicesURL}','${index}','${activity}','${searchInput}',
	                                                                  '${getSelectedServiceDetailsURL}',false,false,true)">
				                                        <label class="custom-control-label" for="cityCheckBox${index}">${key} (${cityData[key]})</label>
				                                    </div>
				                                </div>
				                                `;
		  }
		  index++;
	  }
	  return htmlDataForCityList;
}

function getRenderedListingServices(contextPath,responseData,activity,getSelectedServiceDetailsURL){
	  let htmlDataForListing ='';
	  responseData.filteredSearchServices.forEach(element => {
	  var imagePath;
	  if(cityCheckBoxData.length >= 1 ) 
	  {
		  if(cityCheckBoxData.includes(element.City))
			  {
				imagePath = contextPath + element.Image;
			  	htmlDataForListing += `
				      <div class="d-flex justify-content-between repeat-div">
				      <div class="listing-store-s">
				            <div class="store-details">
				              <div class="d-flex ccsd-flex-wrap">
				                <div class="store-img-de">
				                  <img src='${imagePath}' alt="" class="object-fit-cover">
				                </div>
				                <div class="store-full-de">
				                  <h2 class="font-weight-bold cursor-pointer" onClick="gotoDetailPage('${getSelectedServiceDetailsURL}',${element.Id},'${element.activityTpye}')">${element.Name}</h2>
				                  <div class="d-flex star-r align-items-center cxsd-flex-wrap">
				                    <p>${element.Rating}</p>
				                    <div class="star-icon">
				                    `;
				                    for(let i=1;i<=Math.floor(element.Rating);i++){
				                    	htmlDataForListing += `<span class="star-img fill-star"></span>`;
				                    } for (let i = 5; i>Math.floor(element.Rating); i--){
				                    	htmlDataForListing += `<span class="star-img blank-star"></span>`;
				                    }
				                    htmlDataForListing +=  `</div>
				                    <span class="total-review font-weight-bold">${element.Reviews} reviews</span>
				                  </div>
				                  <div class="">
				                    <address class="store-address">
				                      <p class="ic-phone ic-identy-icon for-before-sprite">${element.Phone}</p>
				                      <p class="ic-address ic-identy-icon for-before-sprite">${element.Address}</p>
				                    </address>`;
				                    if(element.activityTpye == 'Grocery')
				                      htmlDataForListing += `<a href="javascript:void(0)" class="btn-order-now">Order Now</a>`;
				                    else
				                      htmlDataForListing += `<a href="javascript:void(0)" class="btn-order-now">Book Appointment</a>`;	
				             htmlDataForListing += 
				                ` </div>
				                </div>                                      
				              </div>
				            </div>
				          </div>`;
				             if(element.Offer > 0 ){
				             htmlDataForListing += 
					          `<div class="offer-code-s text-center d-flex align-items-center justify-content-center text-white font-weight-bold">
					            <p>${element.Offer}% <br> Discount <br> Offer</p>
					          </div>`;
				             }
				             htmlDataForListing +=`</div>`;
			  } 
	  } else {
		  imagePath = contextPath + element.Image;
		  htmlDataForListing += `
		      <div class="d-flex justify-content-between repeat-div">
		      <div class="listing-store-s">
		            <div class="store-details">
		              <div class="d-flex ccsd-flex-wrap">
		                <div class="store-img-de">
		                  <img src='${imagePath}' alt="" class="object-fit-cover">
		                </div>
		                <div class="store-full-de">
		                  <h2 class="font-weight-bold cursor-pointer" onClick="gotoDetailPage('${getSelectedServiceDetailsURL}',${element.Id},'${element.activityTpye}')">${element.Name}</h2>
		                  <div class="d-flex star-r align-items-center cxsd-flex-wrap">
		                    <p>${element.Rating}</p>
		                    <div class="star-icon">
		                    `;
		                    for(let i=1;i<=Math.floor(element.Rating);i++){
		                    	htmlDataForListing += `<span class="star-img fill-star"></span>`;
		                    } for (let i = 5; i>Math.floor(element.Rating); i--){
		                    	htmlDataForListing += `<span class="star-img blank-star"></span>`;
		                    }
		                    htmlDataForListing +=  `</div>
		                    <span class="total-review font-weight-bold">${element.Reviews} reviews</span>
		                  </div>
		                  <div class="">
		                    <address class="store-address">
		                      <p class="ic-phone ic-identy-icon for-before-sprite">${element.Phone}</p>
		                      <p class="ic-address ic-identy-icon for-before-sprite">${element.Address}</p>
		                   </address>`;
				           if(element.activityTpye == 'Grocery')
				               htmlDataForListing += `<a href="javascript:void(0)" class="btn-order-now">Order Now</a>`;
				           else
				               htmlDataForListing += `<a href="javascript:void(0)" class="btn-order-now">Book Appointment</a>`;	
				           
				          htmlDataForListing += 
				         `</div>
		                </div>                                      
		              </div>
		            </div>
		          </div>`;
		             if(element.Offer > 0 ){
		             htmlDataForListing += 
			          `<div class="offer-code-s text-center d-flex align-items-center justify-content-center text-white font-weight-bold">
			            <p>${element.Offer}% <br> Discount <br> Offer</p>
			          </div>`;
		             }
		             htmlDataForListing +=`</div>`;
	  }
	  });
	  return htmlDataForListing;
}

function gotoDetailPage(getSelectedServiceDetailsURL,serviceId, activity){
	if(activity == undefined)
		activity = '';
	location.href = getSelectedServiceDetailsURL + "&activityType="+activity+"&serviceId=" + serviceId;
}

function applyReviewsFilterOnDetailPage(contextPath,filterReviewsForSelectedServiceURL, activityType, serviceId){
	$.ajax({
		url : filterReviewsForSelectedServiceURL,
		type : 'post',
		datatype : 'json',
		data : {
			"activityType" : activityType,
			"serviceId" : serviceId,
			"filterNumber" : $("#sorting-select").val()
		},
		success : function(data) {
			var responseData = $.parseJSON(data);
			if(responseData.length > 0){
				setReviewsDataInHtml(contextPath,responseData);
			}
		}
	});

}

function setReviewsDataInHtml(contextPath,reviewsData){
	  let htmlData3 = '';  
	  reviewsData.forEach(element => {
	   var data = JSON.stringify(element.ReviewDate);
	   data = new Date(JSON.parse(data));
	   htmlData3 += `<div class="indiv-rating-review">
	                           <div class="d-flex justify-content-between csd-flex-wrap">
	                                <div class="user-info text-center">
	                                    <div class="user-photoes">
	                                        <img src="${contextPath}/assets/images/user_pic_a.png" alt="" class="object-fit-cover">
	                                    </div>
	                                    <div class="user-name">
	                                        <p>${element.Reviewer}</p>                                     
	                                    </div>
	                                </div>
	                                <div class="user-comments">
	                                    <div class="d-flex star-r align-items-center cxsd-flex-wrap">
	                                        <p>${element.ReviewRating}</p>
	                                        <div class="star-icon">`;
	                                        for(let i=0;i<5;i++) { 
	                                         if(Math.floor(element.ReviewRating) > i){
	                                           htmlData3 += `<span class="star-img fill-star"></span>`;
	                                         } else if((Math.floor(element.ReviewRating) != element.ReviewRating) && (Math.floor(element.ReviewRating) == i)){
	                                           htmlData3 += `<span class="star-img half-star"></span>`;
	                                         } else {                                          
	                                           htmlData3 += `<span class="star-img blank-star"></span>`;
	                                         } 
	                                       }
	                                        htmlData3 += `</div>
	                                        <span class="date-time">${data.toString().substr(4,6) + ',' + data.toString().substr(10,6) +  data.toString().substr(16,5) + ' ' + (data.getHours() > 12 ? 'PM' : 'AM')}</span>
	                                    </div>
	                                    <div class="user-exp">
	                                        <p>${element.Comment}</p>
	                                    </div>
	                                    <div class="like-count">
	                                        <p>
	                                            <i class="for-sprite-call ic-like"></i>${element.Like} ${element.Like > 1 ? 'Likes' : 'Like'}
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>`
	  });
	  $('#ratings-commnents-div').html('');
	  $('#ratings-commnents-div').html(htmlData3);
}

function openChatbot(){
	  console.log('jj')
	  $('#chatbot-button').toggle();
	  $('#chatbot').toggle();
	}
