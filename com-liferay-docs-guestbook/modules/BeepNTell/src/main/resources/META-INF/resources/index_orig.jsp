﻿<%@ include file="/init.jsp" %>

<portlet:defineObjects />

<portlet:resourceURL var="fetchSearchSuggestionsURL" id="fetchSearchSuggestions">
</portlet:resourceURL>

<portlet:actionURL name="listingServices" var="listingURL"/>

<!DOCTYPE html>
<html lang="en">
<head>     
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Beepntell</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
    <link rel="icon" type="image/png" href="<%=request.getContextPath()%>/assets/images/favicon.png">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.mCustomScrollbar.css"> 
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">      
</head>
<body>
    <div class="d-flex">
        <div class="left-side-bar">
            <div class="d-none-d cmd-d-block close-p">
                <a href="javascript:void(0)" class="for-sprite-call close-menu"></a>
            </div>
            <div class="mCustomScrollbar scroll-height-side-bar" data-mcs-theme="minimal-dark">
                <div class="filtrar-overlay"></div>
                <ul>
                    <li class="active">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-cab-booking"></i>
                            <span>Cab Booking</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-society-req"></i>
                            <span>Society Visit Request</span>
                        </a>
                    </li>
                      <li>
                        <a href="javascript:void(0)" onclick="getSearchSuggestions('<%=fetchSearchSuggestionsURL%>','Grocery')" title="">
                            <i class="for-sprite-call ic-menu ic-grocery"></i>
                            <span>Grocery</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="getSearchSuggestions('<%=fetchSearchSuggestionsURL%>','Book Services')" title="">
                            <i class="for-sprite-call ic-menu ic-book-service"></i>
                            <span>Book Services</span>
                        </a>
                    </li>
                 
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-book-appo"></i>
                            <span>Book Appointment</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-car-service"></i>
                            <span>Book Car Service</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-news"></i>
                            <span>News</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-rewards"></i>
                            <span>Rewards</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="right-side-bar">
            <main>
                <header>
                    <div class="menu-icon-mob d-none-d cmd-d-block">
                        <a href="javascript:void(0)" class="for-sprite-call ic-mob-icon"></a>
                    </div>
                    <div class="d-flex justify-content-end">
                        <div class="login">
                            <a href="javascript:void(0)" class="d-flex align-items-center">
                                <i class="for-sprite-call ic-user"></i>
                                <span>Log In</span>
                            </a>
                        </div>
                        <div class="category">
                            <a href="javascript:void(0)" class="for-sprite-call ic-category"></a>
                        </div>
                    </div>
                </header>
                <section class="search-part">
                     <div id="ajax_content">Hello ajax 1 </div>

                    <div class="d-none-d csd-d-block mob-cat-div">
                        <div class="owl-carousel owl-theme cat-mob-slider-slider">
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Cab Booking</span>
                                </a>                                
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">                                    
                                    <span>Society Visit Request</span>
                                </a>                              
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Grocery</span>
                                </a>                                
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Book Services</span>
                                </a>
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Book Appointment</span>
                                </a>                               
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">                                    
                                    <span>Book Car Service</span>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="text-center brand-name">
                        <a href="javascript:void(0)" class=""><img src="<%=request.getContextPath()%>/assets/images/logo.svg" alt="Beepntell" class="img-fluid"></a>
                    </div>
                    <div class="search-suggestions open-suggestions position-relative">
                     <form id="searchForm">
                        <div class="input-group ">
                            <input id="searchText" type="text" class="form-control add-suggestions" placeholder="Search for a Service/Product grocery store, saloon, hospital, dance classes etc ">
                            <a href="javascript:void(0)" class="voice-search for-sprite-call"></a>
                            <div class="input-group-append">
                            <a href="<%=listingURL%>">
                              <button class="btn btn-secondary" type="button" onclick="getListingForSearchItem('<%=listingURL%>')">
                                <i class="ic-search for-sprite-call"></i>
                              </button>
                              </a>
                             
                            </div>
                            </form>
                        </div>  
                        <ul class="suggestions-results font-weight-bold">
                        <c:forEach items="${serviceDetails}" var="servieDetail" >
                        <li>
                                <a href="javascript:void(0)" class="for-before-sprite">${servieDetail.Name}</a>
                            </li>
                        </c:forEach>
                        </ul>
                        <div class="text-center bhasha">
                            <p><strong>Beepntell offered in:</strong> <a href="javascript:void(0)">हिन्दी</a> <a href="javascript:void(0)"> বাংলা</a> <a href="javascript:void(0)">తెలుగు</a> <a href="javascript:void(0)">मराठी</a> <a href="javascript:void(0)">தமிழ்</a> <a href="javascript:void(0)">ગુજરાતી</a> <a href="javascript:void(0)">ಕನ್ನಡ</a></p>
                        </div>                        
                    </div>
                </section>
                <section class="offers-reviews">
                    <div class="d-flex justify-content-between clg-flex-wrap">
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Offers &amp; Discount</h2>
                            </div>
                            <div class="owl-carousel owl-theme top-review-slider">
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_01.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">Om Shivay Kirana Store</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>4.0</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>10 reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>20%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_01.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">Om Shivay Kirana Store</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>4.0</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>10 reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>20%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_01.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">Om Shivay Kirana Store</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>4.0</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>10 reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>20%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Recent Reviews</h2>
                            </div>
                            <div class="owl-carousel owl-theme top-review-slider">
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Trending Reviews</h2>
                            </div>
                            <div class="owl-carousel owl-theme top-review-slider">
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                        <p>100 likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                        <p>100 likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="<%=request.getContextPath()%>/assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="<%=request.getContextPath()%>/assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                        <p>100 likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                </section>
            </main>
            <footer>
                <div class="d-flex justify-content-between">
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Promote</a></li>
                        <li><a href="javascript:void(0)">Business Page</a></li>
                        <li><a href="javascript:void(0)">Rewards/Offer</a></li>
                        <li><a href="javascript:void(0)">About</a></li>
                    </ul>
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Privacy</a></li>
                        <li><a href="javascript:void(0)">Terms</a></li>
                        <li><a href="javascript:void(0)">Setting</a></li>
                    </ul>
                </div>
            </footer>
        </div>
    </div>    
    <script src="<%=request.getContextPath()%>/assets/js/jquery-3.js"></script> 
    <script src="<%=request.getContextPath()%>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/owl.carousel.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/index.js"></script>  
      <script src="<%=request.getContextPath()%>/assets/js/myFunctions.js"></script>
      
    
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>     
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Beepntell</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
    <link rel="icon" type="image/png" href="assets/images/favicon.png">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/jquery.mCustomScrollbar.css"> 
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/style.css">      
</head>
<body>
    <div class="d-flex">
        <div class="left-side-bar">
            <div class="d-none-d cmd-d-block close-p">
                <a href="javascript:void(0)" class="for-sprite-call close-menu"></a>
            </div>
            <div class="mCustomScrollbar scroll-height-side-bar" data-mcs-theme="minimal-dark">
                <div class="filtrar-overlay"></div>
                <ul>
                    <li class="active">
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-cab-booking"></i>
                            <span>Cab Booking</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-society-req"></i>
                            <span>Society Visit Request</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-grocery"></i>
                            <span>Grocery</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-book-service"></i>
                            <span>Book Services</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-book-appo"></i>
                            <span>Book Appointment</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-car-service"></i>
                            <span>Book Car Service</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-news"></i>
                            <span>News</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" title="">
                            <i class="for-sprite-call ic-menu ic-rewards"></i>
                            <span>Rewards</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="right-side-bar">
            <main>
                <header>
                    <div class="menu-icon-mob d-none-d cmd-d-block">
                        <a href="javascript:void(0)" class="for-sprite-call ic-mob-icon"></a>
                    </div>
                    <div class="d-flex justify-content-end">
                        <div class="login">
                            <a href="javascript:void(0)" class="d-flex align-items-center">
                                <i class="for-sprite-call ic-user"></i>
                                <span>Log In</span>
                            </a>
                        </div>
                        <div class="category">
                            <a href="javascript:void(0)" class="for-sprite-call ic-category"></a>
                        </div>
                    </div>
                </header>
                <section class="search-part">

                    <div class="d-none-d csd-d-block mob-cat-div">
                        <div class="owl-carousel owl-theme cat-mob-slider-slider">
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Cab Booking</span>
                                </a>                                
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">                                    
                                    <span>Society Visit Request</span>
                                </a>                              
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Grocery</span>
                                </a>                                
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Book Services</span>
                                </a>
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">
                                    <span>Book Appointment</span>
                                </a>                               
                            </div>
                            <div class="item">
                                <a href="javascript:void(0)" title="">                                    
                                    <span>Book Car Service</span>
                                </a>
                            </div>
                        </div>
                    </div>


                    <div class="text-center brand-name">
                        <a href="javascript:void(0)" class=""><img src="assets/images/logo.svg" alt="Beepntell" class="img-fluid"></a>
                    </div>
                    <div class="search-suggestions open-suggestions position-relative">
                        <div class="input-group ">
                            <input type="text" class="form-control add-suggestions" placeholder="Search for a Service/Product grocery store, saloon, hospital, dance classes etc ">
                            <a href="javascript:void(0)" class="voice-search for-sprite-call"></a>
                            <div class="input-group-append">
                              <button class="btn btn-secondary" type="button">
                                <i class="ic-search for-sprite-call"></i>
                              </button>
                            </div>
                        </div>  
                        <ul class="suggestions-results font-weight-bold">
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Om Shivay Kirana Store</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Sumer Chand Tara Chand</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Big Mart Retail Solutions Pvt Ltd</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="for-before-sprite">Easy Day Express </a>
                            </li>
                        </ul>
                        <div class="text-center bhasha">
                            <p><strong>Beepntell offered in:</strong> <a href="javascript:void(0)">हिन्दी</a> <a href="javascript:void(0)"> বাংলা</a> <a href="javascript:void(0)">తెలుగు</a> <a href="javascript:void(0)">मराठी</a> <a href="javascript:void(0)">தமிழ்</a> <a href="javascript:void(0)">ગુજરાતી</a> <a href="javascript:void(0)">ಕನ್ನಡ</a></p>
                        </div>                        
                    </div>
                </section>
                <section class="offers-reviews">
                    <div class="d-flex justify-content-between clg-flex-wrap">
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Offers &amp; Discount</h2>
                            </div>
                            <div class="owl-carousel owl-theme top-review-slider">
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_01.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">Om Shivay Kirana Store</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>4.0</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>10 reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>20%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_01.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">Om Shivay Kirana Store</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>4.0</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>10 reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>20%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_01.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <h2 class="font-weight-bold">Om Shivay Kirana Store</h2>
                                                    <div class="d-flex star-r align-items-center">
                                                        <p>4.0</p>
                                                        <div class="star-icon">
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img fill-star"></span>
                                                            <span class="star-img blank-star"></span>
                                                        </div>
                                                    </div>
                                                    <p>10 reviews</p>
                                                </div>
                                                <div class="right-s">
                                                    <div class="discont-lbl">
                                                        <p>20%</p>
                                                        <p>Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Recent Reviews</h2>
                            </div>
                            <div class="owl-carousel owl-theme top-review-slider">
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                        <div class="offer-review-box">
                            <div class="the-title font-weight-bold">
                                <h2>Trending Reviews</h2>
                            </div>
                            <div class="owl-carousel owl-theme top-review-slider">
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                        <p>100 likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                        <p>100 likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div> 
                                <div class="item">
                                    <div>
                                        <div class="position-relative">
                                            <div class="img-part">
                                                <img src="assets/images/store_img_02.png" alt="" class="object-fit-cover">
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <div class="store-name-review">
                                                    <div class="d-flex user-pic-name align-items-center">
                                                        <div class="user-pic">
                                                            <img src="assets/images/user_pic.png" alt="" class="object-fit-cover">
                                                        </div>
                                                        <div class="user-name">
                                                            <p>Jitender Singh</p>
                                                            <p>Nov 25, 2017 07:01 PM</p>
                                                        </div>
                                                    </div>                                                    
                                                </div>
                                                <div class="right-s">
                                                    <div class="how-m-star">
                                                        <p>4.0 Star</p>
                                                        <p>100 likes</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-name-review box-t">
                                                <h2 class="font-weight-bold">Louis Unisex Salon</h2>
                                                <p>Best for Salon for men...</p>
                                            </div>
                                            <a href="javascript:void(0)" class="full-box-link"></a>
                                        </div>
                                    </div>
                                </div>                                
                            </div>                            
                        </div>
                    </div>
                </section>
            </main>
            <footer>
                <div class="d-flex justify-content-between">
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Promote</a></li>
                        <li><a href="javascript:void(0)">Business Page</a></li>
                        <li><a href="javascript:void(0)">Rewards/Offer</a></li>
                        <li><a href="javascript:void(0)">About</a></li>
                    </ul>
                    <ul class="footer-links">
                        <li><a href="javascript:void(0)">Privacy</a></li>
                        <li><a href="javascript:void(0)">Terms</a></li>
                        <li><a href="javascript:void(0)">Setting</a></li>
                    </ul>
                </div>
            </footer>
        </div>
    </div>    
    <script src="assets/js/jquery-3.js"></script> 
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>    
    <script src="assets/js/bootstrap.js"></script>    
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/index.js"></script>    
    
</body>
</html>