$( document ).ready(function() {
   $('.top-review-slider').owlCarousel({
        loop: true,
        margin:0,       
        items:1,
        dots:true,
        nav:true,
        autoplay:true,
        autoplayTimeout:3500,
        autoplayHoverPause:true,
        responsiveClass: true,
        responsive:{
            320:{
                items:1,
            },
        }
    });

   $('.cat-mob-slider-slider').owlCarousel({
       loop: true,
       margin:5,       
       items:4,
       dots:false,
       nav:false,
       autoplay:false,
       autoplayTimeout:100,
       autoplayHoverPause:false,
       responsiveClass: true,
       responsive:{
           560:{
               items:2.5,
           },
           540:{
               items:2.2,
           },
           500:{
               items:2.1,
           },
           479:{
               items:1.9,
           },
           
           380:{
               items:1.6,
           },
           320:{
               items:1.3,
           },
           210:{
               items:1.2,
           }
       }
   });

    (function($){
        $(window).on("load",function(){
            $(".content").mCustomScrollbar();
        });
    })(jQuery); 
    

    $("#open-filtrar").click(function(){ 
        $("#filtrar-id").toggleClass("show-filtrar-a");  
        $("body").toggleClass("scroll-disable");   
      });
    
      $(".filtrar-overlay").click(function(){ 
        $("#filtrar-id").removeClass("show-filtrar-a"); 
        $("body").removeClass("scroll-disable");     
      });

      $(".inside-fliter").click(function(){ 
        $("#filtrar-id").removeClass("show-filtrar-a"); 
        $("body").removeClass("scroll-disable");     
      });

      

    //search
/*    $(".add-suggestions").click(function(){
        $(".open-suggestions").toggleClass("suggestions-des");
      });
*/    

      $(".menu-icon-mob").click(function(){
        $(".left-side-bar").toggleClass("mob-menu");
        $("body").toggleClass("scroll-disable");
    });

    $(".close-menu").click(function(){
        $(".left-side-bar").toggleClass("mob-menu");
        $("body").toggleClass("scroll-disable");
    });

    $(".filtrar-overlay").click(function(){ 
        $(".left-side-bar").removeClass("mob-menu"); 
        $("body").removeClass("scroll-disable");     
      });

    $('#chatbot').hide();
    $(`#products-div`).hide();
    
    checkMenu();
});

function productsToggle(){
	console.log('hello')
    $(`#products-div`).toggle();
}


function checkMenu(){
	  if($(window).width()<=767){
		  $(`#menu`).hide();
		  $(`#product`).show();
	  } else {
		  $(`#product`).hide();
		  $(`#menu`).show();
	  }
	}

	$(window).resize(function (){
	  checkMenu();
	});
