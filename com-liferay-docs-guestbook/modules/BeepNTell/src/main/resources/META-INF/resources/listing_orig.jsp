<%@ include file="/init.jsp" %>

<portlet:defineObjects />

<portlet:actionURL name="getSelectedServiceDetails" var="getSelectedServiceDetailsURL"/>
<portlet:resourceURL var="filterListedServicesURL" id="filterListedServices"/>

<!DOCTYPE html>
<html lang="en">
<head>     
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Beepntell</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
    <link rel="icon" type="image/png" href="<%=request.getContextPath()%>/assets/images/favicon.png">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.mCustomScrollbar.css"> 
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">  
    <script src="<%=request.getContextPath()%>/assets/js/jquery-3.js"></script> 
    <script src="<%=request.getContextPath()%>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/owl.carousel.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/index.js"></script>  
    <script src="<%=request.getContextPath()%>/assets/js/myFunctions.js"></script>  
</head>
<body>
    <header class="inner-page-header">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="brand-logo">
                    <a href="javascript:void(0)"><img src="assets/images/logo.svg" alt="" class="img-fluid"></a>
                </div>
                <div class="d-flex">
                    <div class="login">
                        <a href="javascript:void(0)" class="d-flex align-items-center">
                            <i class="for-sprite-call ic-user"></i>
                            <span>Log In</span>
                        </a>
                    </div>
                    <div class="category">
                        <a href="javascript:void(0)" class="for-sprite-call ic-category"></a>
                    </div>
                </div>
            </div>
        </div>        
    </header>
    <main>
        <div class="back">
            <div class="container">
                <a href="javascript:void(0)" class="for-before-sprite ic-back-arrow to-home">Grocery Store in Gurgaon,</a>
                <div class="fliter-icon for-sprite-call d-none-d cmd-d-block" id="open-filtrar"></div>
            </div>            
        </div>

        <section class="list-of-stores-es">
            <div class="container">
                <div class="d-flex justify-content-between align-items-start">
                    <div class="left-s-filter" id="filtrar-id">
                        <div class="filtrar-overlay"></div>

                        <div class="fi-inside-div">
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">User Rating</h2>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">5 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">4 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3">3 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                                        <label class="custom-control-label" for="customCheck4">2 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck5">
                                        <label class="custom-control-label" for="customCheck5">1 Star</label>
                                    </div>
                                </div>                            
                            </div>
                            <hr>
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">User Reviews</h2>
                                <div class="form-group">
                                    <div class="custom-control custom-radiobox">
                                        <input type="radio" onchange="getFilteredList('<%=filterListedServicesURL%>')" class="custom-control-input" id="customRadio" name="abc">
                                        <label class="custom-control-label" for="customRadio">5 Star</label>   
                                    </div>                                
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-radiobox">
                                        <input type="radio" class="custom-control-input" id="customRadio2" name="abc">
                                        <label class="custom-control-label" for="customRadio2">4 Star</label>   
                                    </div>                                
                                </div>
                            </div>
                            <hr>
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">City</h2>
                                <div class="form-group">
                                    <input type="text" class="form-ontrol" placeholder="Search by city" name="search">
                                </div>
                                <c:forEach items="${cityMap}" var='cityItem'>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck6">
                                        <label class="custom-control-label" for="customCheck6">${cityItem.key} (${cityItem.value})</label>
                                    </div>
                                </div>
                                </c:forEach>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="link-view-more">View More</a>
                                </div>
                            </div>
                            <hr>
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">Popular Areas</h2>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Delhi</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Noida</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Karol Bagh</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Ghaziabad</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="link-view-more">View More</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="right-s-store-le">
                     <c:forEach items="${searchedservices}" var="servieDetail" >
                        <div  class="d-flex justify-content-between repeat-div">
                            <div class="listing-store-s">
                                <div class="store-details">
                                    <div class="d-flex ccsd-flex-wrap">
                                        <div class="store-img-de">
                                            <img src="<%=request.getContextPath()%>/assets/images/serviceImages/${servieDetail.Image}" alt="" class="object-fit-cover">
                                        </div>
                                        <div class="store-full-de">
                                            <h2 class="font-weight-bold" id="selectedServiceId">${servieDetail.Name}</h2>
                                            <div class="d-flex star-r align-items-center cxsd-flex-wrap">
                                                <p>${servieDetail.Rating} </p>
                                                <div class="star-icon">
                                                    <span class="star-img fill-star"></span>
                                                    <span class="star-img fill-star"></span>
                                                    <span class="star-img fill-star"></span>
                                                    <span class="star-img fill-star"></span>
                                                    <span class="star-img blank-star"></span>
                                                </div>
                                                <span class="total-review font-weight-bold">${servieDetail.Reviews}</span>
                                            </div>
                                            <div class="">
                                                <address class="store-address">
                                                    <p class="ic-phone ic-identy-icon for-before-sprite">${servieDetail.Phone}</p>
                                                    <p class="ic-address ic-identy-icon for-before-sprite">${servieDetail.Address}</p>
                                                </address>
                                                <a href="javascript:void(0)" class="btn-order-now">Order Now</a>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                           <div class="offer-code-s text-center d-flex align-items-center justify-content-center text-white font-weight-bold">
                                <p>20% <br> Discount <br> Offer</p>
                            </div>
                        </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="d-flex justify-content-between">
            <ul class="footer-links">
                <li><a href="javascript:void(0)">Promote</a></li>
                <li><a href="javascript:void(0)">Business Page</a></li>
                <li><a href="javascript:void(0)">Rewards/Offer</a></li>
                <li><a href="javascript:void(0)">About</a></li>
            </ul>
            <ul class="footer-links">
                <li><a href="javascript:void(0)">Privacy</a></li>
                <li><a href="javascript:void(0)">Terms</a></li>
                <li><a href="javascript:void(0)">Setting</a></li>
            </ul>
        </div>
    </footer> 
  </body>
</html>