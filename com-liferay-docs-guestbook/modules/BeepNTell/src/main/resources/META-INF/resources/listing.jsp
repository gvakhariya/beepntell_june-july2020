<%@ include file="/init.jsp" %>

<portlet:defineObjects />

<portlet:actionURL name="getSelectedServiceDetails" var="getSelectedServiceDetailsURL"/>
<portlet:resourceURL var="filterListedServicesURL" id="filterListedServices"/>

<!DOCTYPE html>
<html lang="en">
<head>     
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Beepntell</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet"> 
    <link rel="icon" type="image/png" href="<%=request.getContextPath()%>/assets/images/favicon.png">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/jquery.mCustomScrollbar.css"> 
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/style.css">      
</head>
<body>
    <header class="inner-page-header">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="brand-logo">
                    <a href="javascript:void(0)"><img src="<%=request.getContextPath()%>/assets/images/logo.svg" alt="" class="img-fluid"></a>
                </div>
                <div class="d-flex">
                    <div class="login">
                        <a href="javascript:void(0)" class="d-flex align-items-center">
                            <i class="for-sprite-call ic-user"></i>
                            <span>Log In</span>
                        </a>
                    </div>
                    <div class="category">
                     <a onclick="productsToggle()" href="javascript:void(0)" class="for-sprite-call ic-category"></a>
                            <div id="products-div">
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Visit</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Stay</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Buy</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Concierge</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Book</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Next</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Say</p>
                                </div>
                                <div class="product">
                                    <img class="pro-image" src="<%=request.getContextPath()%>/assets/images/store_img_01.png">
                                    <p>Beep N Ride</p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>        
    </header>
    <main>
        <div class="back">
            <div class="container">
                <a href="javascript:void(0)" class="for-before-sprite ic-back-arrow to-home">Grocery Store in Gurgaon,</a>
                <div class="fliter-icon for-sprite-call d-none-d cmd-d-block" id="open-filtrar"></div>
            </div>            
        </div>

        <section class="list-of-stores-es">
            <div class="container">
                <div class="d-flex justify-content-between align-items-start">
                    <div class="left-s-filter" id="filtrar-id">
                        <div class="filtrar-overlay"></div>

                        <div class="fi-inside-div">
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">User Rating</h2>
                      			<div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="5.0" onchange="applyFilter('<%=request.getContextPath()%>',
                                        '<%=filterListedServicesURL%>','1','${activityType}','${searchText}','<%=getSelectedServiceDetailsURL%>',true,false,false)" 
                                        class="custom-control-input" id="ratingCheckBox1">
                                        <label class="custom-control-label" for="ratingCheckBox1">5 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="4.0" onchange="applyFilter('<%=request.getContextPath()%>',
                                        '<%=filterListedServicesURL%>','2','${activityType}','${searchText}','<%=getSelectedServiceDetailsURL%>',true,false,false)"
                                        class="custom-control-input" id="ratingCheckBox2">
                                        <label class="custom-control-label" for="ratingCheckBox2">>= 4 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="3.0" onchange="applyFilter('<%=request.getContextPath()%>',
                                        '<%=filterListedServicesURL%>','3','${activityType}','${searchText}','<%=getSelectedServiceDetailsURL%>',true,false,false)" 
                                        class="custom-control-input" id="ratingCheckBox3">
                                        <label class="custom-control-label" for="ratingCheckBox3">>= 3 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="2.0" onchange="applyFilter('<%=request.getContextPath()%>',
                                        '<%=filterListedServicesURL%>','4','${activityType}','${searchText}','<%=getSelectedServiceDetailsURL%>',true,false,false)" 
                                        class="custom-control-input" id="ratingCheckBox4">
                                        <label class="custom-control-label" for="ratingCheckBox4">>= 2 Star</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="1.0" onchange="applyFilter('<%=request.getContextPath()%>',
                                        '<%=filterListedServicesURL%>','5','${activityType}','${searchText}','<%=getSelectedServiceDetailsURL%>',true,false,false)" 
                                        class="custom-control-input" id="ratingCheckBox5">
                                        <label class="custom-control-label" for="ratingCheckBox5">>= 1 Star</label>
                                    </div>
                                </div>
                             </div>                           
                            <hr>
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">City</h2>
                                <div class="form-group">
                                    <input type="text" id="citySearchText"
                                     oninput="applyFilter('<%=request.getContextPath()%>','<%=filterListedServicesURL%>','0','${activityType}','${searchText}',
                                     '<%=getSelectedServiceDetailsURL%>',false,true,false)"
                                     class="form-ontrol" placeholder="Search by city" name="search">
                                </div>
                                <div id="cityList">
	                                <c:forEach items="${cityMap}" var='cityItem' varStatus="count">
	                                <div class="form-group">
	                                    <div class="custom-control custom-checkbox">
	                                        <input type="checkbox" class="custom-control-input" id="cityCheckBox${count.index}" value="${cityItem.key}" 
	                                        onchange="applyFilter('<%=request.getContextPath()%>',
                                                                  '<%=filterListedServicesURL%>','${count.index}','${activityType}','${searchText}',
                                                                  '<%=getSelectedServiceDetailsURL%>',false,false,true)">
	                                        <label class="custom-control-label" for="cityCheckBox${count.index}">${cityItem.key} (${cityItem.value})</label>
	                                    </div>
	                                </div>
                                </c:forEach>
                                </div>                                
                            </div>
                            <hr>
                            <div class="name-of-filters">
                                <h2 class="font-weight-bold">Popular Areas</h2>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Delhi</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Noida</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Karol Bagh</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="pop-area-link">Grocery Store in Ghaziabad</a>
                                </div>
                                <div class="form-group">
                                    <a href="javascript:void(0)" class="link-view-more">View More</a>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div id="list-details" class="right-s-store-le">
                       <c:choose>
                       <c:when test="${searchedservices.size() > 0}">
                       <c:forEach items="${searchedservices}" var="servieDetail" >
                        <div class="d-flex justify-content-between repeat-div">
                            <div class="listing-store-s">
                                <div class="store-details">
                                    <div class="d-flex ccsd-flex-wrap">
                                        <div class="store-img-de">
                                           <img src="<%=request.getContextPath()%>${servieDetail.Image}" alt="" class="object-fit-cover">
                                        </div>
                                        <div class="store-full-de">
                                            <h2 class="font-weight-bold  cursor-pointer" onClick="gotoDetailPage('<%= getSelectedServiceDetailsURL%>',
                                            ${servieDetail.Id},'${servieDetail.activityTpye}')">${servieDetail.Name}</h2>
                                            <div class="d-flex star-r align-items-center cxsd-flex-wrap">
                                                <p>${servieDetail.Rating}</p>
                                                <div class="star-icon">
	                                                 <c:forEach begin="1" end="${servieDetail.Rating}" var="i">
	                                        			<span class="star-img fill-star"></span>
					                                 </c:forEach>
					                                 <c:forEach begin="${servieDetail.Rating}" end="4" var="j">
					                                    <span class="star-img blank-star"></span>
					                                 </c:forEach>
                                                </div>
                                                <span class="total-review font-weight-bold">${servieDetail.Reviews} reviews</span>
                                            </div>
                                            <div class="">
                                                <address class="store-address">
                                                    <p class="ic-phone ic-identy-icon for-before-sprite">${servieDetail.Phone}</p>
                                                    <p class="ic-address ic-identy-icon for-before-sprite">${servieDetail.Address}</p>
                                                </address>
                                                <c:choose>
					                                <c:when test="${servieDetail.activityTpye eq 'Grocery'}">
					                                	<a href="javascript:void(0)" class="btn-order-now">Order Now</a>
					                                </c:when>
					                                 <c:otherwise>
					                                	<a href="javascript:void(0)" class="btn-order-now">Book Appointment</a>
					                                </c:otherwise>
					                            </c:choose>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <c:if test="${servieDetail.Offer > 0}">
                            <div class="offer-code-s text-center d-flex align-items-center justify-content-center text-white font-weight-bold">
                                <p>${servieDetail.Offer}% <br> Discount <br> Offer</p>
                            </div>
                            </c:if>
                        </div>
                        </c:forEach>
                        </c:when>
                        <c:otherwise>
                           <h3 class="text-align-center">No Services Found To Show You</h3>
                        </c:otherwise>
                        </c:choose>
                     </div>
                </div>
            </div>
        </section>
    </main>
    <footer>
        <div class="d-flex justify-content-between">
            <ul class="footer-links">
                <li><a href="javascript:void(0)">Promote</a></li>
                <li><a href="javascript:void(0)">Business Page</a></li>
                <li><a href="javascript:void(0)">Rewards/Offer</a></li>
                <li><a href="javascript:void(0)">About</a></li>
            </ul>
            <ul class="footer-links">
                <li><a href="javascript:void(0)">Privacy</a></li>
                <li><a href="javascript:void(0)">Terms</a></li>
                <li><a href="javascript:void(0)">Setting</a></li>
            </ul>
        </div>
    </footer>    
    <script src="<%=request.getContextPath()%>/assets/js/jquery-3.js"></script> 
    <script src="<%=request.getContextPath()%>/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/bootstrap.js"></script>    
    <script src="<%=request.getContextPath()%>/assets/js/owl.carousel.min.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/index.js"></script>
    <script src="<%=request.getContextPath()%>/assets/js/myFunctions.js"></script>      
    
</body>
</html>