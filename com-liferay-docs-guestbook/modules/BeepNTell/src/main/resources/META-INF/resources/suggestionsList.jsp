<%@page import="java.util.Map"%>
<%@ include file="/init.jsp" %>

<portlet:defineObjects />

<%  Map<Integer,String> autoSuggestionsForAllActivites = (Map<Integer,String>)request.getAttribute("autoSuggestionsForAllActivites"); %>

<P>search suggestions</P>

<table class="table table-striped">
<c:forEach items="${autoSuggestionsForAllActivites}" var="Suggestion" >

<tr> 
           <td>${Suggestion.key}</td>
            <td>${Suggestion.value}</td>
            </tr>
</c:forEach>
</table>